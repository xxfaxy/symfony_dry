### 在线演示

地址 http://23.91.101.185:10086

用户名 super

密码 111111111(9个1)

### 该项目能做什么

该项目帮您把后台开发中一般都会有的基础功能都做好了，如果需要增加其他功能，我们提供了非常便利的一整套服务，使得您开发一个功能的增、删、改、查、搜索等只需要10分钟的时间，如果您熟悉了该项目的特性，时间会更短

我们不希望您在后台开发上花费更多精力，而应该把时间花在具体的业务逻辑上

我们不希望您重复地读取数据库或者配置文件然后通过循环的方式生成单选、多选、下拉表单等，而应该在模板里一句代码就搞定，就像下面这样

```
{{ make_choice('status')|raw }}
{{ make_choice('status', 值)|raw }}
```

我们致力于降低symfony框架的学习难度，同时又不损失高并发、高性能的优点

我们适度封装了操作数据库的服务，使得您用起来得心应手，您不必考虑有sql注入的风险，我们在底层帮你做好了防范，您可以看下这个控制器里的代码十分简洁

[点击查看](https://gitee.com/xxfaxy/symfony_dry/blob/master/code/src/Xxfaxy/DryBundle/Controller/UserPermissionController.php)

### 介绍

1.该项目依赖如下软件

linux

nginx

docker(可选)

docker-compose(可选)

php(7.0版本)

mysql(支持其他数据库需要稍作修改)

redis(可选)

2.该项目主要是`symfony`(2.8.27版本)和`INSPINIA - Responsive Admin Theme`(收费软件)的整合

`symfony`官网http://symfony.com

`INSPINIA - Responsive Admin Theme`购买地址http://wrapbootstrap.com/preview/WB0R5L90S

3.该项目默认使用`docker`作为运行环境，如果您不想使用`docker`，`code`目录里就是项目代码，`common/nginx.conf`是`nginx`的配置，请自己修改

4.该项目如果把我写的文档吃透了，难度会比其他框架低很多，而且开发效率也会提升数倍

### 准备工作

1.如果使用docker(推荐)，只需要自己安装好docker和docker-compose即可

2.如果不使用docker，需要自己安装好nginx、php、mysql、redis、composer等软件

### docker方式搭建mysql和redis服务

1.mysql的docker-compose.yml写法

```
version: "2"
services:
    mysql_server:
        image: xxfaxy/3-mysql-server:5.7
        container_name: mysql_server
        ports:
            - "3306:3306"
        volumes:
            - /home/zc/data/disk/mysql:/var/lib/mysql
        environment:
            - MYSQL_ROOT_PASSWORD=1234
        restart: always
```

2. redis的docker-compose.yml写法(可以先跳过)

```
version: "2"
services:
    redis6379:
        image: xxfaxy/self-alpine-redis
        container_name: redis6379
        ports:
            - "6379:6379"
        volumes:
            - "/home/zc/data/disk/redis/redis6379:/data"
        restart: always
```

### 项目运行

1.将`database/symfony_dry.sql`导入mysql数据库`symfony_dry`

2.修改`docker-compose.yml`里面的数据库配置，数据库ip地址`不能`写`127.0.0.1`这样的地址，应该写局域网ip地址，把`SYMFONY__CRUD__CACHE__USE`设置为`false`就不依赖`redis`了

3.进入`code`目录后解压`vendor.tar.gz`

4.在项目根目录运行`./local.sh`，然后输入5，创建镜像

5.在项目根目录运行`./local.sh`，然后输入3，删除容器，如果是第1次运行请忽略错误

6.在项目根目录运行`./local.sh`，然后输入6，运行容器

7.在项目根目录运行`./local.sh`，然后输入8，进入容器，然后在容器里面运行`./tool`，输入1清除缓存

8.浏览器访问`http://127.0.0.1:10008`，输入用户名super，密码111111111(9个1)即可登录后台

### 功能

1.登录用户更改密码

登录后的用户可以更改自己的密码

2.用户组管理

就是用户组的增删改查

3.用户组权限分配

给用户组分配相应的权限

4.用户管理

就是用户的增删改查

5.用户模拟登录

不需要知道用户密码，就可以登录任意用户

6.用户组权限

查看用户组分配到的权限

7.菜单管理

后台的菜单通过菜单管理来动态管理，支持3级菜单

8.设置管理

就是一个键一个值这样的记录管理，可以用来配置网站名称、关键字、电话、电子邮箱等

控制器写法

```
$function = $this->get('service.common.function');
$setting = $function->getSetting();
$oaName = $setting->oa_name;
```

twig模板写法

```
{{ call_method('setting', 'oa_name')|raw }}
```

其中第2个参数就是相应的键值

9.选项管理

给单选、多选、下拉表单提供数据源，如`status`键的值如下

```
[
    {"status":"1","key":"1","value":"启用","note":""},
    {"status":"1","key":"0","value":"禁用","note":""}
]
```

控制器写法

```
$function = $this->get('service.common.function');
$status = $function->getChoice('status');
```

twig模板写法

```
get_choice('status')
```

10.选项配置管理

主要就是用来配置如何重复利用选项管理的数据源及表键值配置的表数据源，快速生成`水平单选` `水平多选` `垂直单选` `垂直多选` `单选下拉` `多选下拉`等表单

twig模板写法

```
{{ make_choice('status')|raw }}
{{ make_choice('status', 值)|raw }}
```

其中第2个参数是编辑数据时数据库保存的值，用来选中之前的选项，如果是多个值，用逗号分割，如'1,2'

上面的写法会自动生成如下代码

```
<div class="form-group">
    <label class="col-sm-2 control-label">状态</label>
    <div class="col-sm-10">
        <label class="radio-inline i-checks"><input type="radio" name="dry_status" value="1" checked="">&nbsp;启用</label>
        <label class="radio-inline i-checks"><input type="radio" name="dry_status" value="0">&nbsp;禁用</label>
    </div>
</div>
```

原理很简单，先从`dry_choice_config`表找到`dry_key`字段值为`status`的记录，然后拿到`dry_choice`字段的值，刚好也是`status`(记为A)，然后从`choice`表找到`dryKey`字段值为`status`(前面的A)的记录，利用里面的数据源生成单选表单

如果`A`的值以`@`符号打头，表示要从表调取数据，如A为`@language_for_permission`，这个时候就与表`choice`没有关系了，它会去`dry_table_key_value`表找到`dry_alias`字段值为`language_for_permission`的记录，然后根据里面的配置知道如何调用表数据，从而生成下拉表单

11.表键值管理

用来配置单选、多选、下拉表单等如何从表调取数据

假设有如下的配置

```
表 dry_language
key字段 dry_key
value字段 dry_key
排序语句 dry_sort asc
```

根据配置会拼接sql语句为`select dry_key as k,dry_key as v from dry_language order by dry_sort asc`，该语句返回的数据有两个字段`k`和`v`

假设数据如下

```
[
    {"k":"1","v":"启用"},
    {"k":"0","v":"禁用"}
]
```

则生成下拉的话会是这样的结构

```
<select>
    <option value="1">启用</option>
    <option value="0">禁用</option>
<select>
```

您也可以写sql语句来满足特别复杂的情况，sql语句需要返回`k`和`v`字段，您可以通过给字段取别名来实现

12.权限管理

就是把整个系统所有的路由都录入进来，形成一个集合，一个路由就是一个权限，有了权限的集合，后续才好分配权限

如果一个功能，假设为`test`功能，相应的路由有`test_index` `test_show` `test_new` `test_edit` `test_delete`，就是增删改查的路由，则创建数据时这样填写数据

```
路由 test
等同菜单路由 空
自动 是
语言 先在语言管理里面创建数据后选择(注意组为crud，请参照着创建)
备注 可以为空
排序 需要小于1000，请查看数字已经用到多少了
```

创建这样一条数据后，由于`自动`选择了`是`，相当于默认添加了上面列出的5个路由

假设后续又增加了`test_data`路由，则需要再创建一条记录，请参考排序数字大于1000的那些记录，需要注意的是`自动`必须选择`否`

等同菜单路由的意思是该路由和哪一个路由在展开菜单时效果是一样的，一般不用填写，请自行体验下各功能，注意菜单的展开情况，特别是`用户组->用户组权限分配`

13.语言管理

就是给一个`键`配置好中文、英文相应的翻译是什么，创建或者编辑数据后，在列表页面需要点`语言生成翻译文件`按钮，生成翻译文件

```
code/src/Xxfaxy/DryBundle/Resources/translations/XxfaxyDryBundle.en_US.yml
code/src/Xxfaxy/DryBundle/Resources/translations/XxfaxyDryBundle.zh_CN.yml
```

twig模板写法

```
{{ dry_translation('list') }}
```

其中参数就是语言的键

14.用户权限管理

给某个用户名赋予或者剔除某个路由的权限，如果状态为禁用，相当于这条记录不生效，也就是相当于没创建过这条记录一样

15.缓存管理

对缓存的管理，如果禁用了缓存，这个功能是不可用的

系统默认支持用redis来缓存数据，首先需要装好redis服务(不是指php的操作类库，php的操作类库项目已经整合好了)，然后在`docker-compose.yml`里配置如下的环境变量

```
- SYMFONY__CRUD__CACHE__USE=true
- SYMFONY__CRUD__CACHE__SERVICE=service.cache
- SYMFONY__CRUD__CACHE__PREFIX=symfony_dry
- SYMFONY__REDIS__HOST=192.168.1.20
- SYMFONY__REDIS__PORT=6390
```

需要注意的是`SYMFONY__REDIS__HOST`不能填写`127.0.0.1`，需要填写局域网ip地址或者公网ip地址

`SYMFONY__CRUD__CACHE__SERVICE`表示用哪一个服务来处理缓存，如果您想用`memcached`，您需要自己写一个服务，实现`service.cache`里面对应的功能即可

`SYMFONY__CRUD__CACHE__PREFIX`表示缓存存储时键的前缀，这样一个redis服务可以提供给多个项目使用，通过前缀来区分项目

控制器写法

```
$crud = $this->get('service.crud')->switch2assoc();
$data1 = $crud->cache(600)->fetch('select * from setting where id=23');
$data2 = $crud->cache('20i', 'xxfaxy')->fetch('select * from setting where id=24');
```

如果您确保您的sql语句没有注入的风险，您可以像上面那样写原生sql语句，否则应该用系统提供的方法查询数据，我们帮您做了防注入功能，请查看`crud服务`章节

上面的`cache`方法有2个参数，第1个参数表示缓存时间，第2个参数表示为缓存取别名，方便后续管理，第2个参数可以省略

缓存时间可以像下面这样写

```
5y
5m
5d
5h
5i
5s
```

分别表示5年、5个月、5天、5小时、5分钟、5秒

也可以直接写整数，如`5`表示5秒，`0`表示此次不读取缓存，直接查询数据库取数据，写`0`和不调用`cache`方法效果是一样的

还可以写`today`表示今天的`23:59:59`这个时间点

上面代码里面的`20i`表示20分钟，缓存策略如下

如果不存在缓存，则查询数据库后把数据缓存起来，如果当前时间是`2017-09-10 10:00:00`，则会记录缓存过期的时间点为`2017-09-10 10:20:00`

如果存在缓存，把当前时间和缓存过期时间比较，如果没有超过过期时间就会直接读取缓存，否则重新查询数据库取数据，同时把数据缓存起来

最后需要注意的是您改了`docker-compose.yml`文件后需要重新运行容器才能生效，方法是在项目根目录下运行`./local.sh`2次，分别输入`3`和`6`

16.重置用户密码

可以重置任意用户名的密码而不需要知道他的密码是多少

17.搜索配置管理

某些功能的列表页面有时需要搜索功能，搜索配置用来自动化完成搜索功能，目前还在开发中

### 配置

1.`code/app/config/config.yml`

parameters.locale 配置地区语言，翻译时根据该值确定系统语言，取值为`zh_CN`和`en_US`

framework.session.handler_id 配置session的存储方式处理服务，这里配置为session.handler.pdo服务，表示把session存储到mysql数据库，如果设置为`~`表示采用默认的存储形式

twig.globals.container 这是增加的全局twig变量，它的值为服务容器

2.`code/app/config/parameters.yml`

该文件主要是配置数据库、后台路径的前缀、缓存、redis、邮件发送等信息

`%变量名%`表示引用变量，这里引用的是环境变量，如果是非docker运行环境，您需要在这里直接写相应的配置

3.`code/app/config/routing.yml`

路由的总调度器，负责导入每个`bundle`的总路由，然后每个`bundle`再导入自己的明细路由

`/`根路径的路由定义也是在该文件定义的

`XxfaxyUserBundle`和`XxfaxyDryBundle`里面的路由都被整体加上了路径前缀

4.`code/app/config/security.yml`

security.firewalls.main.form_login.default_target_path 配置登录后需要去的页面路由

5.`code/app/config/services.yml`

这里面定义了把session存储到mysql的服务

### 登录模板位置

`code/app/Resources/FOSUserBundle/views/layout.html.twig`

### 后台模板继承的base.html.twig模板位置

`code/app/Resources/views/base.html.twig`

### code/composer文件的作用

docker化的php包管理器composer

### code/tool文件的作用

清除项目缓存(主要是twig模板编译缓存)等

### 静态资源存放的位置

`code/web/public`

其中dry目录下的资源是该项目需要的额外的css和javascript脚本等

### HTTPS(SSL)证书的配置

如果是docker运行环境，需要把证书放到`common/ssl`目录下，文件名按规定的命名，然后编辑`common/nginx.conf`文件，取消相应的注释即可

### deploy目录资源的作用

这是将项目用docker部署到生产环境的配置和脚本管理工具

### code/src/Xxfaxy/DryBundle目录说明

一般开发系统常用的功能都放在了该bundle下，后面的章节会详细说明

其中选项、用户组权限、菜单、设置功能采用symfony经典开发方式开发，如果没必要不必关心如下资源

```
code/src/Xxfaxy/DryBundle/Controller/ChoiceController.php(文件)
code/src/Xxfaxy/DryBundle/Controller/GroupPermissionController.php(文件)
code/src/Xxfaxy/DryBundle/Controller/MenuController.php(文件)
code/src/Xxfaxy/DryBundle/Controller/SettingController.php(文件)
code/src/Xxfaxy/DryBundle/Entity(目录)
code/src/Xxfaxy/DryBundle/Form(目录)
code/src/Xxfaxy/DryBundle/Resources/config/doctrine(目录)
```

不建议在该bundle下进行真实开发，您应该新建一个自己的bundle存放项目资源，可以参考如下的命令(您需要进入容器运行)

`/app/app/console generate:bundle --namespace=Company/FunctionBundle --dir=src --format=yml --shared --no-interaction`

### code/src/Xxfaxy/UserBundle目录说明

用户和用户组的相关功能都放在了该bundle下，采用symfony经典开发方式开发，如果没必要不必关心该bundle下的资源

### 开发入门

我们先假定您就在`code/src/Xxfaxy/DryBundle`目录下开发熟悉项目

1.在`code/src/Xxfaxy/DryBundle/Resources/config/routing.yml`里面配置路由(最好是复制里面的配置修改)

2.在`code/src/Xxfaxy/DryBundle/Resources/config/routing`目录新建对应的路由文件(最好是复制别的文件后作相应修改)

3.设计您的表，把表创建好

4.在`code/src/Xxfaxy/DryBundle/Controller`目录新建对应的控制器文件(最好是复制别的文件后作相应修改，别复制之前让您不必关心的控制器，您可以复制`UserPermissionController.php`)

5.把控制器相应的部分作修改

6.在`code/src/Xxfaxy/DryBundle/Resources/views`目录新建对应的模板文件(最好是复制别的文件后作相应修改)

7.后台测试crud是否正常(加app_dev.php实时测试)

8.把相应的语言创建好

9.把相应的权限创建好

### TODO

文档继续编写
