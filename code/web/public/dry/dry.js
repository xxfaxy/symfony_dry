/*
用于拼接html
*/
function bsModal(id, header, body, footer){
    var base = '<div class="modal fade" id="@id"><div class="modal-dialog"><div class="modal-content">@header@body@footer</div></div></div>';
    var _header = (header==''?'':'<div class="modal-header">@</div>'.replace('@', header));
    var _body = (body==''?'':'<div class="modal-body">@</div>'.replace('@', body));
    var _footer = (footer==''?'':'<div class="modal-footer">@</div>'.replace('@', footer));
    return base.replace('@id', id).replace('@header', _header).replace('@body', _body).replace('@footer', _footer);
}

/*
依赖
bsModal
*/
function bsAlert(message){
    var id = 'bsModal';
    var title = '提示';
    var text = '确定';
    var header = '<h4 class="modal-title">@</h4>'.replace('@', title);
    var body = message;
    var footer = '<button type="button" class="btn btn-default" id="alertCloseButton" data-dismiss="modal">@</button>'.replace('@', text);
    var html = bsModal(id, header, body, footer);
    $("body").append(html);
    $('#'+id).modal({backdrop:'static'});
    $("#alertCloseButton").click(function(){
        $('#'+id).remove();
        $('.modal-backdrop').remove();
        $('body').removeClass('modal-open').css("padding-right", 0);
    });
}

function checkPermission(attr, value, flag){
    var selector = "[@attr='@value']".replace('@attr', attr).replace('@value', value);
    $(selector).each(function(){
        var status = $(this).prop('checked');
        if(flag == 1){
            $(this).prop('checked', true);
            $(this).parent().addClass('checked');
        }
        else{
            $(this).prop('checked', !status);
            if(status){
                $(this).parent().removeClass('checked');
            }
            else{
                $(this).parent().addClass('checked');
            }
        }
    });
}

/*
依赖
bsAlert
*/
function submitPermissionForm(){
    if($("#permissionForm").find("input:checked").length == 0){
        bsAlert('权限分配为空');
    }
    else{
        $("#permissionForm").submit();
    }
}

function showDetailModal(content){
    var content = "<pre>" + content + "</pre>";
    if($('#detail_modal')){
        $('#detail_modal').remove();
    }
    var html = '<div class="modal fade" id="detail_modal" tabindex="-1" role="dialog"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">内容如下</h4></div><div class="modal-body">@</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
    html = html.replace('@', content);
    $('body').append(html);
    $('#detail_modal').modal();
}

function arrayIsContain(list, element){
    for(var i = 0;i < list.length;i++){
        if(list[i] == element){
            return true;
        }
    }
    return false;
}

function checkPermissionButton(url){
    var allWillDo = [];
    $("[data-route]").each(function(){
        var temp = $(this).attr('data-route');
        if(! arrayIsContain(allWillDo, temp)){
            allWillDo.push(temp);
        }
    });
    if(allWillDo.length){
        $.post(url, {"data":allWillDo}, function(string){
            var json = JSON.parse(string);
            for(var i in json){
                if(json[i] == 1){
                    $("[data-route='@']".replace('@', i)).show();
                }
            }
        });
    }
}

function go(url){
    window.location.href = url;
}
