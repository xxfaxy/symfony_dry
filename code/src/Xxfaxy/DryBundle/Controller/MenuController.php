<?php
namespace Xxfaxy\DryBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Xxfaxy\DryBundle\Entity\Menu;
use Xxfaxy\DryBundle\Form\MenuType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForNewType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForEditType;

class MenuController extends Controller
{
    public function indexAction(Request $request)
    {
        $twigData = array();
        $function = $this->get('service.common.function');
        $pagination = $this->get('service.pagination');
        $twigData['dryTargetArray'] = $function->getChoice('target');
        $twigData['dryStatusArray'] = $function->getChoice('status');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('XxfaxyDryBundle:Menu')->createQueryBuilder('t');
        $qb = $qb->orderBy('t.dryDataSort', 'asc');
        $total = $qb->select($qb->expr()->count('t'))->getQuery()->getSingleScalarResult();
        $pagesize = 1000;
        $page = $request->query->get('page', 1);
        $offset = ($page-1)*$pagesize;
        $result = $qb->select('t')->setFirstResult($offset)->setMaxResults($pagesize)->getQuery()->getResult();
        $pageCode = $pagination->get(array('total' => $total, 'pageSize' => $pagesize, 'language' => 'cn', 'getParameter' => $request->query->all()));
        $twigData['pageCode'] = $pageCode;
        $twigData['menus'] = $result;
        return $this->render('XxfaxyDryBundle:menu:index.html.twig', $twigData);
    }

    public function newAction(Request $request)
    {
        $function = $this->get('service.common.function');
        $menu = new Menu();
        $form = $this->createForm('Xxfaxy\DryBundle\Form\MenuType', $menu);
        $form->add('SubmitGroup', SubmitGroupForNewType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($menu);
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            $post = $function->getFormData($request->request->all());
            $SubmitGroup = $post['SubmitGroup'];
            if(isset($SubmitGroup['submit'])){
                return $this->redirectToRoute('menu_new');
            }
            else if(isset($SubmitGroup['submitAndShow'])){
                return $this->redirectToRoute('menu_show', array('id' => $menu->getId()));
            }
            else if(isset($SubmitGroup['submitAndList'])){
                return $this->redirectToRoute('menu_index');
            }
            else if(isset($SubmitGroup['submitAndEdit'])){
                return $this->redirectToRoute('menu_edit', array('id' => $menu->getId()));
            }
        }
        return $this->render('XxfaxyDryBundle:menu:new.html.twig', array(
            'menu' => $menu,
            'form' => $form->createView(),
        ));
    }

    public function showAction(Menu $menu)
    {
        $twigData = array();
        $function = $this->get('service.common.function');
        $twigData['dryTargetArray'] = $function->getChoice('target');
        $twigData['dryStatusArray'] = $function->getChoice('status');
        $twigData['menu'] = $menu;
        return $this->render('XxfaxyDryBundle:menu:show.html.twig', $twigData);
    }

    public function editAction(Request $request, Menu $menu)
    {
        $function = $this->get('service.common.function');
        $editForm = $this->createForm('Xxfaxy\DryBundle\Form\MenuType', $menu);
        $editForm->add('SubmitGroup', SubmitGroupForEditType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $editForm->handleRequest($request);
        if($editForm->isSubmitted() && $editForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($menu);
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            $post = $function->getFormData($request->request->all());
            $SubmitGroup = $post['SubmitGroup'];
            if(isset($SubmitGroup['submit'])){
                return $this->redirectToRoute('menu_edit', array('id' => $menu->getId()));
            }
            else if(isset($SubmitGroup['submitAndShow'])){
                return $this->redirectToRoute('menu_show', array('id' => $menu->getId()));
            }
            else if(isset($SubmitGroup['submitAndList'])){
                return $this->redirectToRoute('menu_index');
            }
            else if(isset($SubmitGroup['submitAndNew'])){
                return $this->redirectToRoute('menu_new');
            }
        }
        return $this->render('XxfaxyDryBundle:menu:edit.html.twig', array(
            'menu' => $menu,
            'edit_form' => $editForm->createView(),
        ));
    }

    public function deleteAction(Request $request, Menu $menu)
    {
        $form = $this->createDeleteForm($menu);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($menu);
            $em->flush();
        }
        return $this->redirectToRoute('menu_index');
    }

    private function createDeleteForm(Menu $menu)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('menu_delete', array('id' => $menu->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function updateDataSortAction(Request $request)
    {
        $function = $this->get('service.common.function');
        $crud = $this->get('service.crud');
        $sqlList = $function->getCategoryUpdateSortSql();
        foreach($sqlList as $sql){
            $crud->runUpdate($sql);
        }
        $this->addFlash('message_success', $function->translation('operationSuccess'));
        return $this->redirectToRoute('menu_index');
    }

}
