<?php
namespace Xxfaxy\DryBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SystemController extends Controller
{

    public function homeAction(Request $request)
    {
        return $this->redirectToRoute('fos_user_security_login');
    }

    public function debugAction(Request $request)
    {
        $crud = $this->get('service.crud')->switch2assoc();
        /*列出所有缓存键*/
        $keys = $crud->getCacheKeys('*');
        /*实时数据*/
        $data1 = $crud->fetch('select * from setting where id=23');
        /*3i内有缓存*/
        $data2 = $crud->cache('2i')->fetch('select * from setting where id=23');
        /*3i内有缓存*/
        $data3 = $crud->cache('20i', 'xxfaxy')->fetch('select * from setting where id=24');
        /*实时数据*/
        $data4 = $crud->fetch('select * from setting where id=24');
        /*打印*/
        echo '<pre>';
        print_r($keys);
        print_r($data1);
        print_r($data2);
        print_r($data3);
        print_r($data4);
        exit;
    }

    public function emailAction(Request $request)
    {
        $function = $this->get('service.common.function');
        $subject = '标题->' . date('Y-m-d H:i:s');
        $from = '邮箱';
        $to = '邮箱';
        $body = '<p style="color:red;">test</p>';
        $function->email($subject, $from, $to, $body);
        return new Response('ok');
    }

    public function checkPermissionButtonAction(Request $request)
    {
        $fos = $this->get('service.fos');
        $user = $fos->getUser();
        $data = $request->request->get('data');
        $result = array();
        foreach($data as $item){
            if($fos->checkSecurity($user, $item)){
                $value = 1;
            }
            else{
                $value = 0;
            }
            $result[$item] = $value;
        }
        return new Response(json_encode($result));
    }

    public function resetUserPasswordAction(Request $request)
    {
        if($request->isMethod('POST')){
            $fos = $this->get('service.fos');
            $function = $this->get('service.common.function');
            $post = $request->request->all();
            if(strlen($post['newPassword']) >= 9 && $post['newPassword'] == $post['newRePassword'] && $fos->resetUserPassword($post['username'], $post['newPassword'])){
                $this->addFlash('message_success', $function->translation('operationSuccess'));
            }
            else{
                $this->addFlash('message_error', $function->translation('operationFail'));
            }
        }
        return $this->render('XxfaxyDryBundle:system:resetUserPassword.html.twig');
    }

    public function demoAction(Request $request)
    {
        return $this->render('XxfaxyDryBundle:system:demo.html.twig');
    }

}
