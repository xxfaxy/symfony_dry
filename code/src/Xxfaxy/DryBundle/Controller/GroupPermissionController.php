<?php
namespace Xxfaxy\DryBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Xxfaxy\DryBundle\Entity\GroupPermission;
use Xxfaxy\DryBundle\Form\GroupPermissionType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForNewType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForEditType;

class GroupPermissionController extends Controller
{
    public function indexAction()
    {
        $twigData = array();
        $em = $this->getDoctrine()->getManager();
        $groupPermissions = $em->getRepository('XxfaxyDryBundle:GroupPermission')->findAll();
        $twigData['groupPermissions'] = $groupPermissions;
        return $this->render('XxfaxyDryBundle:grouppermission:index.html.twig', $twigData);
    }

    public function newAction(Request $request)
    {
        $groupPermission = new GroupPermission();
        $form = $this->createForm('Xxfaxy\DryBundle\Form\GroupPermissionType', $groupPermission);
        $form->add('SubmitGroup', SubmitGroupForNewType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($groupPermission);
            $em->flush();
            return $this->redirectToRoute('grouppermission_show', array('id' => $grouppermission->getId()));
        }
        return $this->render('XxfaxyDryBundle:grouppermission:new.html.twig', array(
            'groupPermission' => $groupPermission,
            'form' => $form->createView(),
        ));
    }

    public function showAction(GroupPermission $groupPermission)
    {
        $twigData = array();
        $twigData['groupPermission'] = $groupPermission;
        return $this->render('XxfaxyDryBundle:grouppermission:show.html.twig', $twigData);
    }

    public function editAction(Request $request, GroupPermission $groupPermission)
    {
        $editForm = $this->createForm('Xxfaxy\DryBundle\Form\GroupPermissionType', $groupPermission);
        $editForm->add('SubmitGroup', SubmitGroupForEditType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $editForm->handleRequest($request);
        if($editForm->isSubmitted() && $editForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($groupPermission);
            $em->flush();
            return $this->redirectToRoute('grouppermission_edit', array('id' => $groupPermission->getId()));
        }
        return $this->render('XxfaxyDryBundle:grouppermission:edit.html.twig', array(
            'groupPermission' => $groupPermission,
            'edit_form' => $editForm->createView(),
        ));
    }

    public function deleteAction(Request $request, GroupPermission $groupPermission)
    {
        $form = $this->createDeleteForm($groupPermission);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($groupPermission);
            $em->flush();
        }
        return $this->redirectToRoute('grouppermission_index');
    }

    private function createDeleteForm(GroupPermission $groupPermission)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grouppermission_delete', array('id' => $groupPermission->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
