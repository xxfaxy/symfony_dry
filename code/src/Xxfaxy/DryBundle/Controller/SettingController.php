<?php
namespace Xxfaxy\DryBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Xxfaxy\DryBundle\Entity\Setting;
use Xxfaxy\DryBundle\Form\SettingType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForNewType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForEditType;

class SettingController extends Controller
{
    public function indexAction(Request $request)
    {
        $twigData = array();
        $function = $this->get('service.common.function');
        $pagination = $this->get('service.pagination');
        $twigData['dryUseDetailArray'] = $function->getChoice('yes_or_no');
        $twigData['dryIsJsonStringArray'] = $function->getChoice('yes_or_no');
        $twigData['dryGroupArray'] = $function->getChoice('setting_group');
        $twigData['isCorrectArray'] = $function->getChoice('is_correct');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('XxfaxyDryBundle:Setting')->createQueryBuilder('t');
        $qb = $qb->orderBy('t.drySort', 'asc');
        $total = $qb->select($qb->expr()->count('t'))->getQuery()->getSingleScalarResult();
        $pagesize = 1000;
        $page = $request->query->get('page', 1);
        $offset = ($page-1)*$pagesize;
        $result = $qb->select('t')->setFirstResult($offset)->setMaxResults($pagesize)->getQuery()->getResult();
        $pageCode = $pagination->get(array('total' => $total, 'pageSize' => $pagesize, 'language' => 'cn', 'getParameter' => $request->query->all()));
        $twigData['pageCode'] = $pageCode;
        $twigData['settings'] = $result;
        return $this->render('XxfaxyDryBundle:setting:index.html.twig', $twigData);
    }

    public function newAction(Request $request)
    {
        $function = $this->get('service.common.function');
        $setting = new Setting();
        $form = $this->createForm('Xxfaxy\DryBundle\Form\SettingType', $setting);
        $form->add('SubmitGroup', SubmitGroupForNewType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            if(method_exists($this, 'dataDoForNew')){
                $setting = $this->dataDoForNew($setting);
            }
            $em->persist($setting);
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            $post = $function->getFormData($request->request->all());
            $SubmitGroup = $post['SubmitGroup'];
            if(isset($SubmitGroup['submit'])){
                return $this->redirectToRoute('setting_new');
            }
            else if(isset($SubmitGroup['submitAndShow'])){
                return $this->redirectToRoute('setting_show', array('id' => $setting->getId()));
            }
            else if(isset($SubmitGroup['submitAndList'])){
                return $this->redirectToRoute('setting_index');
            }
            else if(isset($SubmitGroup['submitAndEdit'])){
                return $this->redirectToRoute('setting_edit', array('id' => $setting->getId()));
            }
        }
        return $this->render('XxfaxyDryBundle:setting:new.html.twig', array(
            'setting' => $setting,
            'form' => $form->createView(),
        ));
    }

    public function showAction(Setting $setting)
    {
        $twigData = array();
        $function = $this->get('service.common.function');
        $settingGroup = $function->getChoice('setting_group');
        $twigData['dryUseDetailArray'] = $function->getChoice('yes_or_no');
        $twigData['dryIsJsonStringArray'] = $function->getChoice('yes_or_no');
        $twigData['dryGroupArray'] = $settingGroup;
        $twigData['setting'] = $setting;
        return $this->render('XxfaxyDryBundle:setting:show.html.twig', $twigData);
    }

    public function editAction(Request $request, Setting $setting)
    {
        $function = $this->get('service.common.function');
        $editForm = $this->createForm('Xxfaxy\DryBundle\Form\SettingType', $setting);
        $editForm->add('SubmitGroup', SubmitGroupForEditType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $editForm->handleRequest($request);
        if($editForm->isSubmitted() && $editForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            if(method_exists($this, 'dataDoForEdit')){
                $setting = $this->dataDoForEdit($setting);
            }
            $em->persist($setting);
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            $post = $function->getFormData($request->request->all());
            $SubmitGroup = $post['SubmitGroup'];
            if(isset($SubmitGroup['submit'])){
                return $this->redirectToRoute('setting_edit', array('id' => $setting->getId()));
            }
            else if(isset($SubmitGroup['submitAndShow'])){
                return $this->redirectToRoute('setting_show', array('id' => $setting->getId()));
            }
            else if(isset($SubmitGroup['submitAndList'])){
                return $this->redirectToRoute('setting_index');
            }
            else if(isset($SubmitGroup['submitAndNew'])){
                return $this->redirectToRoute('setting_new');
            }
        }
        return $this->render('XxfaxyDryBundle:setting:edit.html.twig', array(
            'setting' => $setting,
            'edit_form' => $editForm->createView(),
        ));
    }

    public function deleteAction(Request $request, Setting $setting)
    {
        $form = $this->createDeleteForm($setting);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($setting);
            $em->flush();
        }
        return $this->redirectToRoute('setting_index');
    }

    private function createDeleteForm(Setting $setting)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('setting_delete', array('id' => $setting->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
