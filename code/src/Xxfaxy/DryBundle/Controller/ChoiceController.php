<?php
namespace Xxfaxy\DryBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Xxfaxy\DryBundle\Entity\Choice;
use Xxfaxy\DryBundle\Form\ChoiceType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForNewType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForEditType;

class ChoiceController extends Controller
{
    public function indexAction(Request $request)
    {
        $twigData = array();
        $function = $this->get('service.common.function');
        $pagination = $this->get('service.pagination');
        $twigData['dryStatusArray'] = $function->getChoice('status');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('XxfaxyDryBundle:Choice')->createQueryBuilder('t');
        $qb = $qb->orderBy('t.drySort', 'asc');
        $total = $qb->select($qb->expr()->count('t'))->getQuery()->getSingleScalarResult();
        $pagesize = 1000;
        $page = $request->query->get('page', 1);
        $offset = ($page-1)*$pagesize;
        $result = $qb->select('t')->setFirstResult($offset)->setMaxResults($pagesize)->getQuery()->getResult();
        $pageCode = $pagination->get(array('total' => $total, 'pageSize' => $pagesize, 'language' => 'cn', 'getParameter' => $request->query->all()));
        $twigData['pageCode'] = $pageCode;
        $twigData['choices'] = $result;
        return $this->render('XxfaxyDryBundle:choice:index.html.twig', $twigData);
    }

    public function newAction(Request $request)
    {
        $function = $this->get('service.common.function');
        $choice = new Choice();
        $form = $this->createForm('Xxfaxy\DryBundle\Form\ChoiceType', $choice);
        $form->add('SubmitGroup', SubmitGroupForNewType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($choice);
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            $post = $function->getFormData($request->request->all());
            $SubmitGroup = $post['SubmitGroup'];
            if(isset($SubmitGroup['submit'])){
                return $this->redirectToRoute('choice_new');
            }
            else if(isset($SubmitGroup['submitAndShow'])){
                return $this->redirectToRoute('choice_show', array('id' => $choice->getId()));
            }
            else if(isset($SubmitGroup['submitAndList'])){
                return $this->redirectToRoute('choice_index');
            }
            else if(isset($SubmitGroup['submitAndEdit'])){
                return $this->redirectToRoute('choice_edit', array('id' => $choice->getId()));
            }
        }
        return $this->render('XxfaxyDryBundle:choice:new.html.twig', array(
            'choice' => $choice,
            'form' => $form->createView(),
        ));
    }

    public function showAction(Choice $choice)
    {
        $twigData = array();
        $function = $this->get('service.common.function');
        $twigData['dryStatusArray'] = $function->getChoice('status');
        $twigData['choice'] = $choice;
        return $this->render('XxfaxyDryBundle:choice:show.html.twig', $twigData);
    }

    public function editAction(Request $request, Choice $choice)
    {
        $function = $this->get('service.common.function');
        $editForm = $this->createForm('Xxfaxy\DryBundle\Form\ChoiceType', $choice);
        $editForm->add('SubmitGroup', SubmitGroupForEditType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $editForm->handleRequest($request);
        if($editForm->isSubmitted() && $editForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($choice);
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            $post = $function->getFormData($request->request->all());
            $SubmitGroup = $post['SubmitGroup'];
            if(isset($SubmitGroup['submit'])){
                return $this->redirectToRoute('choice_edit', array('id' => $choice->getId()));
            }
            else if(isset($SubmitGroup['submitAndShow'])){
                return $this->redirectToRoute('choice_show', array('id' => $choice->getId()));
            }
            else if(isset($SubmitGroup['submitAndList'])){
                return $this->redirectToRoute('choice_index');
            }
            else if(isset($SubmitGroup['submitAndNew'])){
                return $this->redirectToRoute('choice_new');
            }
        }
        return $this->render('XxfaxyDryBundle:choice:edit.html.twig', array(
            'choice' => $choice,
            'edit_form' => $editForm->createView(),
        ));
    }

    public function deleteAction(Request $request, Choice $choice)
    {
        $form = $this->createDeleteForm($choice);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($choice);
            $em->flush();
        }
        return $this->redirectToRoute('choice_index');
    }

    private function createDeleteForm(Choice $choice)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('choice_delete', array('id' => $choice->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
