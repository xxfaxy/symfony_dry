<?php
namespace Xxfaxy\DryBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LanguageController extends Controller
{

    public function indexAction(Request $request)
    {
        $crud = $this->get('service.crud');
        $sql = $this->get('service.sql');
        $pagination = $this->get('service.pagination');
        $page = $request->query->getInt('page', 1);
        $dryGroup = $request->query->get('dry_group', 'crud');
        $pageSize = 1000;
        $sql->table('dry_language');
        $sql->where('dry_group', '=', $dryGroup);
        $sql->order('dry_sort', 'asc');
        $sql->limit($page, $pageSize);
        $list = $crud->fetchAll($sql->get());
        $total = $crud->fetchColumn($sql->getTotalSql());
        $data = array(
            'groups' => $crud->fetchAll('select distinct dry_group from dry_language'),
            'dryGroup' => $dryGroup,
            'list' => $list,
            'total' => $total,
            'pageCode' => $pagination->get(array('total' => $total, 'pageSize' => $pageSize, 'language' => 'cn', 'getParameter' => $request->query->all()))
        );
        return $this->render('XxfaxyDryBundle:language:index.html.twig', $data);
    }

    public function showAction(Request $request, $id)
    {
        $crud = $this->get('service.crud');
        $rs = $crud->one('dry_language', $id);
        $data = array(
            'rs' => $rs
        );
        return $this->render('XxfaxyDryBundle:language:show.html.twig', $data);
    }

    public function newAction(Request $request)
    {
        $function = $this->get('service.common.function');
        $crud = $this->get('service.crud');
        if($request->isMethod('POST')){
            $post = $request->request->all();
            $post['dry_add_time'] = date('Y-m-d H:i:s');
            $crud->insert('dry_language', $post);
            $this->addFlash('message_success', $function->translation('operationSuccess'));
        }
        return $this->render('XxfaxyDryBundle:language:new.html.twig');
    }

    public function editAction(Request $request, $id)
    {
        $function = $this->get('service.common.function');
        $crud = $this->get('service.crud');
        if($request->isMethod('POST')){
            $post = $request->request->all();
            $crud->update('dry_language', $post, $id);
            $this->addFlash('message_success', $function->translation('operationSuccess'));
        }
        $rs = $crud->one('dry_language', $id);
        $data = array(
            'rs' => $rs
        );
        return $this->render('XxfaxyDryBundle:language:edit.html.twig', $data);
    }

    public function deleteAction(Request $request, $id)
    {

    }

    public function makeAction(Request $request)
    {
        $function = $this->get('service.common.function');
        $crud = $this->get('service.crud');
        $bundlePath = $this->getParameter('bundle_path');
        $list = $crud->fetchAll('select * from dry_language order by dry_sort asc');
        $languageList = array(
            'en_US',
            'zh_CN'
        );
        foreach($languageList as $language){
            $this->makeOne($list, 'dry_'.strtolower($language), "{$bundlePath}/Resources/translations/XxfaxyDryBundle.{$language}.yml");
        }
        $this->addFlash('message_success', $function->translation('operationSuccess'));
        return $this->redirectToRoute('language_index');
    }

    private function makeOne($list, $field, $file)
    {
        $yaml = $this->get('service.parse.yaml');
        $result = array();
        foreach($list as $v){
            $result[$v['dry_key']] = $v[$field];
        }
        file_put_contents($file, $yaml->dump($result));
    }

}
