<?php
namespace Xxfaxy\DryBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TableKeyValueController extends Controller
{

    public function indexAction(Request $request)
    {
        $crud = $this->get('service.crud');
        $sql = $this->get('service.sql');
        $pagination = $this->get('service.pagination');
        $page = $request->query->getInt('page', 1);
        $pageSize = 10;
        $sql->table('dry_table_key_value');
        $sql->order('dry_sort', 'asc');
        $sql->limit($page, $pageSize);
        $list = $crud->fetchAll($sql->get());
        $total = $crud->fetchColumn($sql->getTotalSql());
        $data = array(
            'list' => $list,
            'total' => $total,
            'pageCode' => $pagination->get(array('total' => $total, 'pageSize' => $pageSize, 'language' => 'cn', 'getParameter' => $request->query->all()))
        );
        return $this->render('XxfaxyDryBundle:tablekeyvalue:index.html.twig', $data);
    }

    public function showAction(Request $request, $id)
    {
        $crud = $this->get('service.crud');
        $rs = $crud->one('dry_table_key_value', $id);
        $data = array(
            'rs' => $rs
        );
        return $this->render('XxfaxyDryBundle:tablekeyvalue:show.html.twig', $data);
    }

    public function newAction(Request $request)
    {
        $function = $this->get('service.common.function');
        $crud = $this->get('service.crud');
        if($request->isMethod('POST')){
            $post = $request->request->all();
            $post['dry_add_time'] = date('Y-m-d H:i:s');
            $crud->insert('dry_table_key_value', $post);
            $this->addFlash('message_success', $function->translation('operationSuccess'));
        }
        return $this->render('XxfaxyDryBundle:tablekeyvalue:new.html.twig');
    }

    public function editAction(Request $request, $id)
    {
        $function = $this->get('service.common.function');
        $crud = $this->get('service.crud');
        if($request->isMethod('POST')){
            $post = $request->request->all();
            $crud->update('dry_table_key_value', $post, $id);
            $this->addFlash('message_success', $function->translation('operationSuccess'));
        }
        $rs = $crud->one('dry_table_key_value', $id);
        $data = array(
            'rs' => $rs
        );
        return $this->render('XxfaxyDryBundle:tablekeyvalue:edit.html.twig', $data);
    }

    public function deleteAction(Request $request, $id)
    {

    }

}
