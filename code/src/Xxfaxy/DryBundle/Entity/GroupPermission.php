<?php
namespace Xxfaxy\DryBundle\Entity;

/**
 * GroupPermission
 */
class GroupPermission
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $dryRouteName;

    /**
     * @var \DateTime
     */
    private $dryAddtime;

    /**
     * @var string
     */
    private $dryNote;

    /**
     * @var \Xxfaxy\UserBundle\Entity\Group
     */
    private $dryGroup;

    public function __construct()
    {
        $this->dryAddtime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dryRouteName
     *
     * @param string $dryRouteName
     *
     * @return GroupPermission
     */
    public function setDryRouteName($dryRouteName)
    {
        $this->dryRouteName = $dryRouteName;
        return $this;
    }

    /**
     * Get dryRouteName
     *
     * @return string
     */
    public function getDryRouteName()
    {
        return $this->dryRouteName;
    }

    /**
     * Set dryAddtime
     *
     * @param \DateTime $dryAddtime
     *
     * @return GroupPermission
     */
    public function setDryAddtime($dryAddtime)
    {
        $this->dryAddtime = $dryAddtime;
        return $this;
    }

    /**
     * Get dryAddtime
     *
     * @return \DateTime
     */
    public function getDryAddtime()
    {
        return $this->dryAddtime;
    }

    public function getDryAddtimeString()
    {
        return $this->dryAddtime->format('Y-m-d H:i:s');
    }

    /**
     * Set dryNote
     *
     * @param string $dryNote
     *
     * @return GroupPermission
     */
    public function setDryNote($dryNote)
    {
        $this->dryNote = $dryNote;
        return $this;
    }

    /**
     * Get dryNote
     *
     * @return string
     */
    public function getDryNote()
    {
        return $this->dryNote;
    }

    /**
     * Set dryGroup
     *
     * @param \Xxfaxy\UserBundle\Entity\Group $dryGroup
     *
     * @return GroupPermission
     */
    public function setDryGroup(\Xxfaxy\UserBundle\Entity\Group $dryGroup = null)
    {
        $this->dryGroup = $dryGroup;
        return $this;
    }

    /**
     * Get dryGroup
     *
     * @return \Xxfaxy\UserBundle\Entity\Group
     */
    public function getDryGroup()
    {
        return $this->dryGroup;
    }

}
