<?php
namespace Xxfaxy\DryBundle\Entity;

/**
 * Menu
 */
class Menu
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dryGrade;

    /**
     * @var string
     */
    private $dryIcon;

    /**
     * @var string
     */
    private $dryName;

    /**
     * @var string
     */
    private $dryRouteName;

    /**
     * @var string
     */
    private $dryUrl;

    /**
     * @var string
     */
    private $dryTarget;

    /**
     * @var integer
     */
    private $dryStatus;

    /**
     * @var string
     */
    private $dryNote;

    /**
     * @var integer
     */
    private $drySort;

    /**
     * @var string
     */
    private $dryDataSort;

    /**
     * @var \DateTime
     */
    private $dryAddtime;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dryChild;

    /**
     * @var \Xxfaxy\DryBundle\Entity\Menu
     */
    private $dryParent;

    public function __toString()
    {
        return str_repeat('-', ($this->getDryGrade()-1)*5).$this->getDryName();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dryChild = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dryAddtime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dryGrade
     *
     * @param integer $dryGrade
     *
     * @return Menu
     */
    public function setDryGrade($dryGrade)
    {
        $this->dryGrade = $dryGrade;
        return $this;
    }

    /**
     * Get dryGrade
     *
     * @return integer
     */
    public function getDryGrade()
    {
        return $this->dryGrade;
    }

    /**
     * Set dryIcon
     *
     * @param string $dryIcon
     *
     * @return Menu
     */
    public function setDryIcon($dryIcon)
    {
        $this->dryIcon = $dryIcon;
        return $this;
    }

    /**
     * Get dryIcon
     *
     * @return string
     */
    public function getDryIcon()
    {
        return $this->dryIcon;
    }

    /**
     * Set dryName
     *
     * @param string $dryName
     *
     * @return Menu
     */
    public function setDryName($dryName)
    {
        $this->dryName = $dryName;
        return $this;
    }

    /**
     * Get dryName
     *
     * @return string
     */
    public function getDryName()
    {
        return $this->dryName;
    }

    /**
     * Set dryRouteName
     *
     * @param string $dryRouteName
     *
     * @return Menu
     */
    public function setDryRouteName($dryRouteName)
    {
        $this->dryRouteName = $dryRouteName;
        return $this;
    }

    /**
     * Get dryRouteName
     *
     * @return string
     */
    public function getDryRouteName()
    {
        return $this->dryRouteName;
    }

    /**
     * Set dryUrl
     *
     * @param string $dryUrl
     *
     * @return Menu
     */
    public function setDryUrl($dryUrl)
    {
        $this->dryUrl = $dryUrl;
        return $this;
    }

    /**
     * Get dryUrl
     *
     * @return string
     */
    public function getDryUrl()
    {
        return $this->dryUrl;
    }

    /**
     * Set dryTarget
     *
     * @param string $dryTarget
     *
     * @return Menu
     */
    public function setDryTarget($dryTarget)
    {
        $this->dryTarget = $dryTarget;
        return $this;
    }

    /**
     * Get dryTarget
     *
     * @return string
     */
    public function getDryTarget()
    {
        return $this->dryTarget;
    }

    /**
     * Set dryStatus
     *
     * @param integer $dryStatus
     *
     * @return Menu
     */
    public function setDryStatus($dryStatus)
    {
        $this->dryStatus = $dryStatus;
        return $this;
    }

    /**
     * Get dryStatus
     *
     * @return integer
     */
    public function getDryStatus()
    {
        return $this->dryStatus;
    }

    /**
     * Set dryNote
     *
     * @param string $dryNote
     *
     * @return Menu
     */
    public function setDryNote($dryNote)
    {
        $this->dryNote = $dryNote;
        return $this;
    }

    /**
     * Get dryNote
     *
     * @return string
     */
    public function getDryNote()
    {
        return $this->dryNote;
    }

    /**
     * Set drySort
     *
     * @param integer $drySort
     *
     * @return Menu
     */
    public function setDrySort($drySort)
    {
        $this->drySort = $drySort;
        return $this;
    }

    /**
     * Get drySort
     *
     * @return integer
     */
    public function getDrySort()
    {
        return $this->drySort;
    }

    /**
     * Set dryDataSort
     *
     * @param string $dryDataSort
     *
     * @return Menu
     */
    public function setDryDataSort($dryDataSort)
    {
        $this->dryDataSort = $dryDataSort;
        return $this;
    }

    /**
     * Get dryDataSort
     *
     * @return string
     */
    public function getDryDataSort()
    {
        return $this->dryDataSort;
    }

    /**
     * Set dryAddtime
     *
     * @param \DateTime $dryAddtime
     *
     * @return Menu
     */
    public function setDryAddtime($dryAddtime)
    {
        $this->dryAddtime = $dryAddtime;
        return $this;
    }

    /**
     * Get dryAddtime
     *
     * @return \DateTime
     */
    public function getDryAddtime()
    {
        return $this->dryAddtime;
    }

    public function getDryAddtimeString()
    {
        return $this->dryAddtime->format('Y-m-d H:i:s');
    }

    /**
     * Add dryChild
     *
     * @param \Xxfaxy\DryBundle\Entity\Menu $dryChild
     *
     * @return Menu
     */
    public function addDryChild(\Xxfaxy\DryBundle\Entity\Menu $dryChild)
    {
        $this->dryChild[] = $dryChild;
        return $this;
    }

    /**
     * Remove dryChild
     *
     * @param \Xxfaxy\DryBundle\Entity\Menu $dryChild
     */
    public function removeDryChild(\Xxfaxy\DryBundle\Entity\Menu $dryChild)
    {
        $this->dryChild->removeElement($dryChild);
    }

    /**
     * Get dryChild
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDryChild()
    {
        return $this->dryChild;
    }

    /**
     * Set dryParent
     *
     * @param \Xxfaxy\DryBundle\Entity\Menu $dryParent
     *
     * @return Menu
     */
    public function setDryParent(\Xxfaxy\DryBundle\Entity\Menu $dryParent = null)
    {
        $this->dryParent = $dryParent;
        return $this;
    }

    /**
     * Get dryParent
     *
     * @return \Xxfaxy\DryBundle\Entity\Menu
     */
    public function getDryParent()
    {
        return $this->dryParent;
    }

}
