<?php
namespace Xxfaxy\DryBundle\Entity;

/**
 * Setting
 */
class Setting
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $dryGroup;

    /**
     * @var string
     */
    private $dryName;

    /**
     * @var string
     */
    private $dryValue;

    /**
     * @var integer
     */
    private $drySort;

    /**
     * @var \DateTime
     */
    private $dryAddtime;

    /**
     * @var string
     */
    private $dryNote;

    /**
     * @var string
     */
    private $dryUseDetail;

    /**
     * @var string
     */
    private $dryIsJsonString;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dryAddtime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dryGroup
     *
     * @param string $dryGroup
     *
     * @return Setting
     */
    public function setDryGroup($dryGroup)
    {
        $this->dryGroup = $dryGroup;
        return $this;
    }

    /**
     * Get dryGroup
     *
     * @return string
     */
    public function getDryGroup()
    {
        return $this->dryGroup;
    }

    /**
     * Set dryName
     *
     * @param string $dryName
     *
     * @return Setting
     */
    public function setDryName($dryName)
    {
        $this->dryName = $dryName;
        return $this;
    }

    /**
     * Get dryName
     *
     * @return string
     */
    public function getDryName()
    {
        return $this->dryName;
    }

    /**
     * Set dryValue
     *
     * @param string $dryValue
     *
     * @return Setting
     */
    public function setDryValue($dryValue)
    {
        $this->dryValue = $dryValue;
        return $this;
    }

    /**
     * Get dryValue
     *
     * @return string
     */
    public function getDryValue()
    {
        return $this->dryValue;
    }

    /**
     * Set drySort
     *
     * @param integer $drySort
     *
     * @return Setting
     */
    public function setDrySort($drySort)
    {
        $this->drySort = $drySort;
        return $this;
    }

    /**
     * Get drySort
     *
     * @return integer
     */
    public function getDrySort()
    {
        return $this->drySort;
    }

    /**
     * Set dryAddtime
     *
     * @param \DateTime $dryAddtime
     *
     * @return Setting
     */
    public function setDryAddtime($dryAddtime)
    {
        $this->dryAddtime = $dryAddtime;
        return $this;
    }

    /**
     * Get dryAddtime
     *
     * @return \DateTime
     */
    public function getDryAddtime()
    {
        return $this->dryAddtime;
    }

    public function getDryAddtimeString()
    {
        return $this->dryAddtime->format('Y-m-d H:i:s');
    }

    /**
     * Set dryNote
     *
     * @param string $dryNote
     *
     * @return Setting
     */
    public function setDryNote($dryNote)
    {
        $this->dryNote = $dryNote;
        return $this;
    }

    /**
     * Get dryNote
     *
     * @return string
     */
    public function getDryNote()
    {
        return $this->dryNote;
    }

    /**
     * Set dryUseDetail
     *
     * @param string $dryUseDetail
     *
     * @return Setting
     */
    public function setDryUseDetail($dryUseDetail)
    {
        $this->dryUseDetail = $dryUseDetail;
        return $this;
    }

    /**
     * Get dryUseDetail
     *
     * @return string
     */
    public function getDryUseDetail()
    {
        return $this->dryUseDetail;
    }

    /**
     * Set dryIsJsonString
     *
     * @param string $dryIsJsonString
     *
     * @return Setting
     */
    public function setDryIsJsonString($dryIsJsonString)
    {
        $this->dryIsJsonString = $dryIsJsonString;
        return $this;
    }

    /**
     * Get dryIsJsonString
     *
     * @return string
     */
    public function getDryIsJsonString()
    {
        return $this->dryIsJsonString;
    }

}
