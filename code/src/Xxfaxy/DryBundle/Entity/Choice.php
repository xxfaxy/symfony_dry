<?php
namespace Xxfaxy\DryBundle\Entity;

/**
 * Choice
 */
class Choice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $dryName;

    /**
     * @var string
     */
    private $dryKey;

    /**
     * @var string
     */
    private $dryValue;

    /**
     * @var integer
     */
    private $drySort;

    /**
     * @var \DateTime
     */
    private $dryAddtime;

    /**
     * @var integer
     */
    private $dryStatus;

    /**
     * @var string
     */
    private $dryNote;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dryChild;

    /**
     * @var \Xxfaxy\DryBundle\Entity\Choice
     */
    private $dryParent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dryChild = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dryAddtime = new \DateTime();
    }

    public function __toString()
    {
        return ''.$this->getDryName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dryName
     *
     * @param string $dryName
     *
     * @return Choice
     */
    public function setDryName($dryName)
    {
        $this->dryName = $dryName;
        return $this;
    }

    /**
     * Get dryName
     *
     * @return string
     */
    public function getDryName()
    {
        return $this->dryName;
    }

    /**
     * Set dryKey
     *
     * @param string $dryKey
     *
     * @return Choice
     */
    public function setDryKey($dryKey)
    {
        $this->dryKey = $dryKey;
        return $this;
    }

    /**
     * Get dryKey
     *
     * @return string
     */
    public function getDryKey()
    {
        return $this->dryKey;
    }

    /**
     * Set dryValue
     *
     * @param string $dryValue
     *
     * @return Choice
     */
    public function setDryValue($dryValue)
    {
        $this->dryValue = $dryValue;
        return $this;
    }

    /**
     * Get dryValue
     *
     * @return string
     */
    public function getDryValue()
    {
        return $this->dryValue;
    }

    /**
     * Set drySort
     *
     * @param integer $drySort
     *
     * @return Choice
     */
    public function setDrySort($drySort)
    {
        $this->drySort = $drySort;
        return $this;
    }

    /**
     * Get drySort
     *
     * @return integer
     */
    public function getDrySort()
    {
        return $this->drySort;
    }

    /**
     * Set dryAddtime
     *
     * @param \DateTime $dryAddtime
     *
     * @return Choice
     */
    public function setDryAddtime($dryAddtime)
    {
        $this->dryAddtime = $dryAddtime;
        return $this;
    }

    /**
     * Get dryAddtime
     *
     * @return \DateTime
     */
    public function getDryAddtime()
    {
        return $this->dryAddtime;
    }

    public function getDryAddtimeString()
    {
        return $this->dryAddtime->format('Y-m-d H:i:s');
    }

    /**
     * Set dryStatus
     *
     * @param integer $dryStatus
     *
     * @return Choice
     */
    public function setDryStatus($dryStatus)
    {
        $this->dryStatus = $dryStatus;
        return $this;
    }

    /**
     * Get dryStatus
     *
     * @return integer
     */
    public function getDryStatus()
    {
        return $this->dryStatus;
    }

    /**
     * Set dryNote
     *
     * @param string $dryNote
     *
     * @return Choice
     */
    public function setDryNote($dryNote)
    {
        $this->dryNote = $dryNote;
        return $this;
    }

    /**
     * Get dryNote
     *
     * @return string
     */
    public function getDryNote()
    {
        return $this->dryNote;
    }

    /**
     * Add dryChild
     *
     * @param \Xxfaxy\DryBundle\Entity\Choice $dryChild
     *
     * @return Choice
     */
    public function addDryChild(\Xxfaxy\DryBundle\Entity\Choice $dryChild)
    {
        $this->dryChild[] = $dryChild;
        return $this;
    }

    /**
     * Remove dryChild
     *
     * @param \Xxfaxy\DryBundle\Entity\Choice $dryChild
     */
    public function removeDryChild(\Xxfaxy\DryBundle\Entity\Choice $dryChild)
    {
        $this->dryChild->removeElement($dryChild);
    }

    /**
     * Get dryChild
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDryChild()
    {
        return $this->dryChild;
    }

    /**
     * Set dryParent
     *
     * @param \Xxfaxy\DryBundle\Entity\Choice $dryParent
     *
     * @return Choice
     */
    public function setDryParent(\Xxfaxy\DryBundle\Entity\Choice $dryParent = null)
    {
        $this->dryParent = $dryParent;
        return $this;
    }

    /**
     * Get dryParent
     *
     * @return \Xxfaxy\DryBundle\Entity\Choice
     */
    public function getDryParent()
    {
        return $this->dryParent;
    }

}
