<?php
namespace Xxfaxy\DryBundle\Services\Extension;

class TwigExtension extends \Twig_Extension
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function getTests()
    {
        return array(
            new \Twig_SimpleTest('is_json_string', function($value){
                $bool = is_array(json_decode($value, true));
                if($bool){
                    return 1;
                }
                else{
                    return 0;
                }
            }),
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('call_service_method', function(){
                $args = func_get_args();
                $parameter = array_slice($args, 2);
                $service = $this->container->get($args[0]);
                return call_user_func_array(array($service, $args[1]), $parameter);
            }),
            new \Twig_SimpleFunction('call_method', function(){
                $args = func_get_args();
                $parameter = array_slice($args, 1);
                $service = $this->container->get('service.for.twig');
                return call_user_func_array(array($service, $args[0]), $parameter);
            }),
            new \Twig_SimpleFunction('form_auto', function($context){
                $service = $this->container->get('service.form.auto');
                return $service->all($context);
            }),
            new \Twig_SimpleFunction('dry_path', function($route, $id = 0){
                $router = $this->container->get('router');
                if($id){
                    return $router->generate($route, array('id' => $id));
                }
                else{
                    return $router->generate($route);
                }
            }),
            new \Twig_SimpleFunction('make_choice', function($choiceConfigKey, $default = ''){
                $make = $this->container->get('service.make');
                return $make->makeChoice($choiceConfigKey, $default);
            }),
            new \Twig_SimpleFunction('data_source', function($service, $function, $parameter){
                $dataSource = $this->container->get("service.data.source.{$service}");
                return $dataSource->{$function}($parameter);
            }),
            new \Twig_SimpleFunction('dry_translation', function($key){
                $function = $this->container->get('service.common.function');
                return $function->translation($key);
            }),
            new \Twig_SimpleFunction('get_choice', function($key){
                $function = $this->container->get('service.common.function');
                return $function->getChoice($key);
            }),
            new \Twig_SimpleFunction('get_choice_from_table', function($key){
                $make = $this->container->get('service.make');
                return $make->getDataFromTableKeyValue($key, true);
            }),
            new \Twig_SimpleFunction('today', function($span = 0){
                return date('Y-m-d');
            }),
            new \Twig_SimpleFunction('show_from_table', function($table, $field, $id){
                $crud = $this->container->get('service.crud')->switch2obj();
                $list = $crud->fetchAll("select concat_ws('->',{$field}) as name from {$table} where id in ({$id})");
                $html = array();
                foreach($list as $v){
                    $html[] = $v->name;
                }
                return implode(PHP_EOL, $html);
            }),
        );
    }

    /*
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('test', function(){
            }),
        );
    }
    */

    public function getName()
    {
        return 'service_twig_extension';
    }

}
