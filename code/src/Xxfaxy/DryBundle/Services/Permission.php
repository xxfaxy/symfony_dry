<?php
namespace Xxfaxy\DryBundle\Services;

class Permission
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    /*view*/
    public function getPermission($isAuto = true)
    {
        $result = array();
        $crud = $this->container->get('service.crud')->switch2obj();
        if($isAuto){
            $list = $crud->fetchAll("select * from dry_permission where dry_auto='yes' order by dry_sort asc");
            foreach($list as $v){
                $result[$v->dry_route][] = array('language' => $v->dry_language, 'route' => $v->dry_route.'_index', 'group' => 'index');
                $result[$v->dry_route][] = array('language' => $v->dry_language, 'route' => $v->dry_route.'_show', 'group' => 'show');
                $result[$v->dry_route][] = array('language' => $v->dry_language, 'route' => $v->dry_route.'_new', 'group' => 'new');
                $result[$v->dry_route][] = array('language' => $v->dry_language, 'route' => $v->dry_route.'_edit', 'group' => 'edit');
                $result[$v->dry_route][] = array('language' => $v->dry_language, 'route' => $v->dry_route.'_delete', 'group' => 'delete');
            }
        }
        else{
            $list = $crud->fetchAll("select * from dry_permission where dry_auto='no' order by dry_sort asc");
            foreach($list as $v){
                $result[] = array('language' => $v->dry_language, 'route' => $v->dry_route);
            }
        }
        return $result;
    }

}
