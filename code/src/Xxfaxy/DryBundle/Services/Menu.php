<?php
namespace Xxfaxy\DryBundle\Services;

class Menu
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function fetchAll($sql)
    {
        $crud = $this->container->get('service.crud')->switch2assoc();
        return $crud->fetchAll($sql);
    }

    public function getMenuForUser()
    {
        $result = array();
        $fos = $this->container->get('service.fos');
        $user = $fos->getUser();
        if(! method_exists($user, 'getId')){
            return $result;
        }
        $info = $fos->getUserInformation($user->getId());
        if($info['is_super_admin']){
            $sql = '
            select
            m.*,
            pp.id as dryParentParent
            from menu m
            left join menu p on p.id=m.dryParent
            left join menu pp on pp.id=p.dryParent
            where m.dryStatus=1
            order by m.dryDataSort asc
            ';
            $result = $this->fetchAll($sql);
            return $result;
        }
        $sql = '
        select
        m.dryRouteName,
        m.id,
        m.dryParent,
        pp.id as dryParentParent
        from menu m
        left join menu c on c.dryParent=m.id
        left join menu p on p.id=m.dryParent
        left join menu pp on pp.id=p.dryParent
        where m.dryStatus=1 and c.id is null
        ';
        $list = $this->fetchAll($sql);
        foreach($list as $k => $v){
            if(! in_array($v['dryRouteName'], $info['route_list'])){
                unset($list[$k]);
            }
        }
        $idList = array();
        if(! empty($list)){
            foreach($list as $v){
                $idList[] = $v['id'];
                if($v['dryParent'] != ''){
                    $idList[] = $v['dryParent'];
                }
                if($v['dryParentParent'] != ''){
                    $idList[] = $v['dryParentParent'];
                }
            }
            $idList = array_unique($idList);
            $idList = implode(',', $idList);
            $sql = "
            select
            m.*,
            pp.id as dryParentParent
            from menu m
            left join menu p on p.id=m.dryParent
            left join menu pp on pp.id=p.dryParent
            where m.id in ({$idList}) and m.dryStatus=1
            order by m.dryDataSort asc
            ";
            $result = $this->fetchAll($sql);
        }
        return $result;
    }

    public function getMenuForGrade()
    {
        $result = array();
        $list = $this->getMenuForUser();
        foreach($list as $k => $v){
            if($v['dryGrade'] == 1){
                $result[$v['id']] = $v;
            }
            else if($v['dryGrade'] == 2){
                $result[$v['dryParent']]['children'][$v['id']] = $v;
            }
            else if($v['dryGrade'] == 3){
                $result[$v['dryParentParent']]['children'][$v['dryParent']]['children'][$v['id']] = $v;
            }
        }
        return $result;
    }

    public function getMenuForLi($data)
    {
        $result = array();
        $result[] = '<li>';
        if(isset($data['children'])){
            $result[] = '<a href="javascript:void(0);">';
        }
        else{
            if($data['dryRouteName'] != ''){
                $router = $this->container->get('router');
                if(is_null($router->getRouteCollection()->get($data['dryRouteName']))){
                    return '';
                }
                $href = $router->generate($data['dryRouteName']);
                $_route = $data['dryRouteName'];
            }
            else{
                $href = $data['dryUrl'];
                $_route = '';
            }
            $result[] = "<a href='{$href}' target='{$data['dryTarget']}' routeName='{$_route}'>";
        }
        if($data['dryIcon'] != ''){
            $result[] = "<i class='{$data['dryIcon']}'></i>";
        }
        $space = str_repeat('&nbsp;', $data['dryGrade'] - 1);
        if($data['dryGrade'] == 1){
            $result[] = "{$space}<span class='nav-label'>{$data['dryName']}</span>";
        }
        else{
            $result[] = "{$space}{$data['dryName']}";
        }
        if(isset($data['children'])){
            $result[] = '<span class="fa arrow"></span>';
        }
        $result[] = '</a>';
        if(isset($data['children'])){
            if($data['dryGrade'] == 1){
                $result[] = '<ul class="nav nav-second-level collapse">';
            }
            if($data['dryGrade'] == 2){
                $result[] = '<ul class="nav nav-third-level">';
            }
            foreach($data['children'] as $v){
                $result[] = $this->getMenuForLi($v);
            }
            $result[] = '</ul>';
        }
        $result[] = '</li>';
        $result = implode('', $result);
        return $result;
    }

    public function getMenuForHtml()
    {
        $result = array();
        $list = $this->getMenuForGrade();
        foreach($list as $v){
            $result[] = $this->getMenuForLi($v);
        }
        $result = implode('', $result);
        return $result;
    }

}
