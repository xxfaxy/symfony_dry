<?php
namespace Xxfaxy\DryBundle\Services\Listener;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\Common\Util\Debug;

class CheckPermissionListener
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        /*$request = $event->getRequest();*/
        $request = $this->container->get('request');
        $fos = $this->container->get('service.fos');
        $token = $this->container->get('security.token_storage')->getToken();
        $_route = $request->get('_route');
        if($token){
            $user = $token->getUser();
            if(method_exists($user, 'getId')){
                if(!$fos->checkSecurity($user, $_route)){
                    //throw new AccessDeniedException();
                    exit('AccessDenied');
                }
            }
        }
    }

}
