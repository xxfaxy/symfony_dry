<?php
namespace Xxfaxy\DryBundle\Services;

class Cache
{

    protected $container;

    private $redis = null;

    private $prefix = '';

    public function __construct($service_container)
    {
        $this->container = $service_container;
        $this->redis = $this->container->get('service.redis')->get();
    }

    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    private function keyAddPrefix($key)
    {
        return $this->prefix.'_'.$key;
    }

    public function set($key, $value)
    {
        $key = $this->keyAddPrefix($key);
        $this->redis->set($key, serialize($value));
    }

    public function get($key, $isFull = false)
    {
        if(! $isFull){
            $key = $this->keyAddPrefix($key);
        }
        return unserialize($this->redis->get($key));
    }

    public function has($key, $isFull = false)
    {
        if(! $isFull){
            $key = $this->keyAddPrefix($key);
        }
        return $this->redis->exists($key);
    }

    public function remove($key, $isFull = false)
    {
        if(! $isFull){
            $key = $this->keyAddPrefix($key);
        }
        $this->redis->del($key);
    }

    public function keys($pattern = '*')
    {
        $key = $this->keyAddPrefix($pattern);
        return $this->redis->keys($key);
    }

}
