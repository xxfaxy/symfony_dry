<?php
namespace Xxfaxy\DryBundle\Services;

class Pdo
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function get()
    {
        return $this->container->get('doctrine.orm.default_entity_manager')->getConnection()->getWrappedConnection();
    }

}
