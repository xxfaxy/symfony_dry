<?php
namespace Xxfaxy\DryBundle\Services;

class Encrypt
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function encrypt($data, $key, $iv)
    {
        $key = hex2bin($key);
        $iv = hex2bin($iv);
        return openssl_encrypt($data, 'aes-128-cbc', $key, 0, $iv);
    }

    public function decrypt($data, $key, $iv)
    {
        $key = hex2bin($key);
        $iv = hex2bin($iv);
        return openssl_decrypt($data, 'aes-128-cbc', $key, 0, $iv);
    }

    public function getArrayByString($string, $key, $iv)
    {
        $string = str_replace(' ', '+', $string);
        return json_decode($this->decrypt($string, $key, $iv), true);
    }

    public function getStringByArray($array, $key, $iv)
    {
        return $this->encrypt(json_encode($array, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES), $key, $iv);
    }

}
