<?php
namespace Xxfaxy\DryBundle\Services;

class Twig
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function choiceShow($keyValue, $data)
    {
        if(!is_array($data)){
            $data = array($data);
        }
        $result = array();
        foreach($data as $v){
            $result[] = $keyValue[$v];
        }
        return implode(PHP_EOL, $result);
    }

    public function groupShow($groups)
    {
        $result = array();
        foreach($groups as $v){
            $result[] = $v->getName();
        }
        return implode(PHP_EOL, $result);
    }

    public function arrayShow($array)
    {
        return implode(PHP_EOL, $array);
    }

    public function gradeShow($text, $grade)
    {
        $html = str_repeat('&nbsp;&nbsp;&nbsp;', $grade-1).$text;
        $list = array(
            '1' => "@text",
            '2' => "<span class='category_grade_2'>@text</span>",
            '3' => "<span class='category_grade_3'>@text</span>",
            '4' => "<span class='category_grade_4'>@text</span>"
        );
        $html = str_replace('@text', $html, $list[$grade]);
        return $html;
    }

    public function detailShow($data)
    {
        if($data == ''){
            return '';
        }
        $html = '<button type="button" class="btn btn-info the_detail_button">@detail</button><textarea style="display:none;">@text</textarea>';
        $html = str_replace('@text', $data, $html);
        $html = str_replace('@detail', '详情', $html);
        return $html;
    }

    public function statusShow($keyValue, $data)
    {
        $list = array(
            '0' => "<span class='label label-danger'>@text</span>",
            '1' => "<span class='label label-primary'>@text</span>",
            'no' => "<span class='label label-danger'>@text</span>",
            'yes' => "<span class='label label-primary'>@text</span>",
        );
        $html = str_replace('@text', $keyValue[$data], $list[$data]);
        return $html;
    }

    public function setting($name)
    {
        $function = $this->container->get('service.common.function');
        $setting = $function->getSetting();
        return $setting->{$name};
    }

    public function resetRouteName($route)
    {
        $crud = $this->container->get('service.crud');
        $temp = explode('_', $route);
        if(count($temp) == 2 && in_array($temp[1], array('show', 'new', 'edit'))){
            $temp[1] = 'index';
            return implode('_', $temp);
        }
        else{
            $dryMenuRoute = $crud->fetchColumn("select dry_menu_route from dry_permission where dry_route='{$route}' and dry_auto='no'");
            if($dryMenuRoute){
                return $dryMenuRoute;
            }
            else{
                return $route;
            }
        }
    }

}
