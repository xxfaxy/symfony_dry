<?php
namespace Xxfaxy\DryBundle\Services;

class Redis
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function get()
    {
        $host = $this->container->getParameter('redis_host');
        $port = $this->container->getParameter('redis_port');
        $config = array(
            'host' => $host,
            'port' => $port,
            'database' => 0
        );
        $redis = new \Predis\Client($config);
        return $redis;
    }

}
