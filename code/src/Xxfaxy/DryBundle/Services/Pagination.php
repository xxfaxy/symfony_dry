<?php
namespace Xxfaxy\DryBundle\Services;

class Pagination
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    /*view*/
    public function get($array)
    {
        /**/
        $total = $array['total'];
        $pageSize = $array['pageSize'];
        $language = $array['language'];
        $getParameter = $array['getParameter'];
        if(isset($getParameter['page'])){
            $page = $getParameter['page'];
            unset($getParameter['page']);
        }
        else{
            $page = 1;
        }
        /**/
        if(empty($getParameter)){
            $parameter = '';
        }
        else{
            $parameter = array();
            foreach($getParameter as $k => $v){
                $parameter[] = "{$k}={$v}";
            }
            $parameter = '&'.implode('&', $parameter);
        }
        /**/
        $config = array(
            'firstPageText' => array('en' => 'First page', 'cn' => '首页'),
            'lastPageText' => array('en' => 'Last page', 'cn' => '尾页'),
            'prevPageText' => array('en' => 'Prev page', 'cn' => '上一页'),
            'nextPageText' => array('en' => 'Next page', 'cn' => '下一页')
        );
        $firstPageText = $config['firstPageText'][$language];
        $lastPageText = $config['lastPageText'][$language];
        $prevPageText = $config['prevPageText'][$language];
        $nextPageText = $config['nextPageText'][$language];
        /*左右偏移页码数*/
        $span = 5;
        /*总页数*/
        $totalPage = ceil($total/$pageSize);
        /*all*/
        $all = range($page-$span, $page+$span);
        /*html*/
        $html = array("<ul class='pagination'>");
        /**/
        $prev = $page-1;
        $next = $page+1;
        if($page==1){
            $html[] = "<li class='footable-page-arrow disabled'><a data-page='first' href='javascript:void(0);'>{$firstPageText}</a></li>";
            $html[] = "<li class='footable-page-arrow disabled'><a data-page='prev' href='javascript:void(0);'>{$prevPageText}</a></li>";
        }
        else{
            $html[] = "<li class='footable-page-arrow'><a data-page='first' href='?page=1{$parameter}'>{$firstPageText}</a></li>";
            $html[] = "<li class='footable-page-arrow'><a data-page='prev' href='?page={$prev}{$parameter}'>{$prevPageText}</a></li>";
        }
        foreach($all as $k => $v){
            if($v>=1 && $v<=$totalPage){
                $index = $v-1;
                if($v==$page){
                    $html[] = "<li class='footable-page active disabled'><a data-page='{$index}' href='javascript:void(0);'>{$v}</a></li>";
                }
                else{
                    $html[] = "<li class='footable-page'><a data-page='{$index}' href='?page={$v}{$parameter}'>{$v}</a></li>";
                }
            }
        }
        if($page==$totalPage){
            $html[] = "<li class='footable-page-arrow disabled'><a data-page='next' href='javascript:void(0);'>{$nextPageText}</a></li>";
            $html[] = "<li class='footable-page-arrow disabled'><a data-page='last' href='javascript:void(0);'>{$lastPageText}</a></li>";
        }
        else{
            $html[] = "<li class='footable-page-arrow'><a data-page='next' href='?page={$next}{$parameter}'>{$nextPageText}</a></li>";
            $html[] = "<li class='footable-page-arrow'><a data-page='last' href='?page={$totalPage}{$parameter}'>{$lastPageText}</a></li>";
        }
        $html[] = '</ul>';
        return implode('', $html);
    }

}
