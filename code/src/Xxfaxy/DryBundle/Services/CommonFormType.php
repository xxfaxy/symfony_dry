<?php
namespace Xxfaxy\DryBundle\Services;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommonFormType extends AbstractType
{

    protected $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('container' => $this->container));
    }

    public function getName()
    {
        return 'container_aware';
    }

    public function getParent()
    {
        return 'form';
    }

}
