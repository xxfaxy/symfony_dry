<?php
namespace Xxfaxy\DryBundle\Services;

class FormAuto
{

    public $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function all($context)
    {
        $dryMethod = $context['attr']['dryMethod'];
        $entity = $context['attr']['entity'];
        return $this->{$dryMethod}($context, $entity);
    }

    public function groupRolesRadio($context, $entity)
    {
        if($entity->getId()){
            $default = $entity->getRoles();
        }
        else{
            $default = array('ROLE_USER');
        }
        $twig = $this->container->get('twig');
        $function = $this->container->get('service.common.function');
        $list = array();
        foreach($function->getChoice('role') as $k => $v){
            $list[] = array('key' => $k, 'value' => $v);
        }
        $data = array(
            'name' => 'roles',
            'default' => $default,
            'list' => $list
        );
        return $twig->render("XxfaxyDryBundle:_system_/form:radio.html.twig", $data);
    }

}
