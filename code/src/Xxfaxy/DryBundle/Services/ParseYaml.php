<?php
namespace Xxfaxy\DryBundle\Services;
use Symfony\Component\Yaml\Yaml;

class ParseYaml
{

    protected $container;

    public function __construct($service_container)
    {
        $this->container = $service_container;
    }

    public function load($filename)
    {
        $array = Yaml::parse(file_get_contents($filename));
        return $array;
    }

    public function dump($array)
    {
        return Yaml::dump($array);
    }

}
