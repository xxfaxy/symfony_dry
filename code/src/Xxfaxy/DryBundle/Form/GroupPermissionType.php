<?php
namespace Xxfaxy\DryBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupPermissionType extends AbstractType
{

    public function getParent()
    {
        return 'container_aware';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dryGroup', null, array('label' => 'GroupPermission.dryGroup','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryRouteName',null,array('label'=>'GroupPermission.dryRouteName','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryNote',null,array('label'=>'GroupPermission.dryNote','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Xxfaxy\DryBundle\Entity\GroupPermission',
            'translation_domain' => 'XxfaxyDryBundle',
        ));
    }

    public function getName()
    {
        return 'xxfaxy_drybundle_grouppermission';
    }

}
