<?php
namespace Xxfaxy\DryBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChoiceType extends AbstractType
{

    public function getParent()
    {
        return 'container_aware';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
        $function = $container->get('service.common.function');
        $status = $function->getChoice('status');
        $builder->add('dryParent', null, array('label' => 'Choice.dryParent','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control'),'query_builder'=>function($em){$qb=$em->createQueryBuilder('t');return $qb->where($qb->expr()->isNull('t.dryParent'));}));
        $builder->add('dryName',null,array('label'=>'Choice.dryName','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryKey',null,array('label'=>'Choice.dryKey','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryValue','textarea',array('label'=>'Choice.dryValue','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control','style'=>'height:200px;')));
        $builder->add('drySort',null,array('label'=>'Choice.drySort','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryStatus', 'choice',array('choices'=>$status,'multiple'=>false,'label'=>'Choice.dryStatus','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryNote',null,array('label'=>'Choice.dryNote','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Xxfaxy\DryBundle\Entity\Choice',
            'translation_domain' => 'XxfaxyDryBundle',
        ));
    }

    public function getName()
    {
        return 'xxfaxy_drybundle_choice';
    }

}
