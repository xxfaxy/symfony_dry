<?php
namespace Xxfaxy\DryBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FormType;

class SubmitGroupForEditType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }

    public function getParent()
    {
        return FormType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('submit','submit',array('label'=>'submit','translation_domain'=>'XxfaxyDryBundle','attr'=>array('class'=>'btn btn-primary')))
            ->add('submitAndShow','submit',array('label'=>'submitAndShow','translation_domain'=>'XxfaxyDryBundle','attr'=>array('class'=>'btn btn-primary')))
            ->add('submitAndList','submit',array('label'=>'submitAndList','translation_domain'=>'XxfaxyDryBundle','attr'=>array('class'=>'btn btn-primary')))
            ->add('submitAndNew','submit',array('label'=>'submitAndNew','translation_domain'=>'XxfaxyDryBundle','attr'=>array('class'=>'btn btn-primary')))
        ;
    }

}
