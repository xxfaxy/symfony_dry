<?php
namespace Xxfaxy\DryBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Xxfaxy\DryBundle\Form\Type\MarkDownType;

class SettingType extends AbstractType
{

    public function getParent()
    {
        return 'container_aware';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
        $function = $container->get('service.common.function');
        $yesOrNo = $function->getChoice('yes_or_no');
        $settingGroup = $function->getChoice('setting_group');
        $builder->add('dryGroup', 'choice',array('choices'=>$settingGroup,'multiple'=>false,'label'=>'Setting.dryGroup','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryName',null,array('label'=>'Setting.dryName','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryValue',null,array('label'=>'Setting.dryValue','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control','style'=>'height:300px;')));
        $builder->add('drySort',null,array('label'=>'Setting.drySort','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryNote',null,array('label'=>'Setting.dryNote','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryUseDetail', 'choice',array('choices'=>$yesOrNo,'multiple'=>false,'label'=>'Setting.dryUseDetail','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryIsJsonString', 'choice',array('choices'=>$yesOrNo,'multiple'=>false,'label'=>'Setting.dryIsJsonString','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Xxfaxy\DryBundle\Entity\Setting',
            'translation_domain' => 'XxfaxyDryBundle',
        ));
    }

    public function getName()
    {
        return 'xxfaxy_drybundle_setting';
    }

}
