<?php
namespace Xxfaxy\DryBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuType extends AbstractType
{

    public function getParent()
    {
        return 'container_aware';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
        $function = $container->get('service.common.function');
        $target = $function->getChoice('target');
        $status = $function->getChoice('status');
        $builder->add('dryParent', null, array('label' => 'Menu.dryParent','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control'),'query_builder'=>function($em){$qb=$em->createQueryBuilder('t');return $qb->where('t.dryGrade<=2')->orderBy('t.dryDataSort','asc');}));
        $builder->add('dryGrade',null,array('label'=>'Menu.dryGrade','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryIcon',null,array('label'=>'Menu.dryIcon','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryName',null,array('label'=>'Menu.dryName','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryRouteName',null,array('label'=>'Menu.dryRouteName','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryUrl',null,array('label'=>'Menu.dryUrl','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryTarget', 'choice',array('choices'=>$target,'multiple'=>false,'label'=>'Menu.dryTarget','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryStatus', 'choice',array('choices'=>$status,'multiple'=>false,'label'=>'Menu.dryStatus','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('dryNote',null,array('label'=>'Menu.dryNote','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
        $builder->add('drySort',null,array('label'=>'Menu.drySort','label_attr'=>array('class'=>'control-label col-sm-2'),'attr'=>array('class'=>'form-control')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Xxfaxy\DryBundle\Entity\Menu',
            'translation_domain' => 'XxfaxyDryBundle',
        ));
    }

    public function getName()
    {
        return 'xxfaxy_drybundle_menu';
    }

}
