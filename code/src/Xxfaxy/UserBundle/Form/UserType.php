<?php
namespace Xxfaxy\UserBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    public function getParent()
    {
        return 'container_aware';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
        $function = $container->get('service.common.function');
        $status = $function->getChoice('status');
        $builder->add('groups', null, array('label' => 'User.groups', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control')));
        $builder->add('username', null, array('label' => 'User.username', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control')));
        $builder->add('email', null, array('label' => 'User.email', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control')));
        $builder->add('enabled', 'choice', array('choices' => $status, 'multiple' => false, 'label' => 'User.enabled', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control')));
        $builder->add('password', null, array('label' => 'User.password', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Xxfaxy\UserBundle\Entity\User',
            'translation_domain' => 'XxfaxyDryBundle',
        ));
    }

    public function getName()
    {
        return 'xxfaxy_userbundle_user';
    }

}
