<?php
namespace Xxfaxy\UserBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Xxfaxy\DryBundle\Form\Type\AutoType;

class GroupType extends AbstractType
{

    public function getParent()
    {
        return 'container_aware';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
        $entity = $builder->getData();
        $builder->add('name', null, array('label' => 'Group.name', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control')));
        $builder->add('roles', AutoType::class, array('label' => 'Group.roles', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('dryMethod' => 'groupRolesRadio', 'entity' => $entity), 'mapped' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Xxfaxy\UserBundle\Entity\Group',
            'translation_domain' => 'XxfaxyDryBundle',
        ));
    }

    public function getName()
    {
        return 'xxfaxy_userbundle_group';
    }

}
