<?php
namespace Xxfaxy\UserBundle\Entity;
use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

class Group extends BaseGroup
{
    protected $id;

    private $users;

    public function __construct()
    {
        parent::__construct(array());
        $this->users = new ArrayCollection();
    }

    public function __tostring()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function addUser(User $users)
    {
        $this->users[] = $users;
        return $this;
    }

    public function removeUser(User $users)
    {
        $this->users->removeElement($users);
    }

    public function getUsers()
    {
        return $this->users;
    }

}
