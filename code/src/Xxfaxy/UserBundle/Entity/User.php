<?php
namespace Xxfaxy\UserBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\GroupInterface;
use Doctrine\Common\Collections\ArrayCollection;

class User extends BaseUser
{
    protected $id;

    protected $groups;

    private $createdAt;

    public $locked;

    public $expired;

    public $expiresAt;

    public $credentialsExpired;

    public $credentialsExpireAt;

    public function __construct()
    {
        parent::__construct();
        $this->groups = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->username;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRoles()
    {
        $roles = $this->roles;
        foreach($this->getGroups() as $group){
            if($group->getRoles()){
                $roles = array_merge($roles, $group->getRoles());
            }
        }
        $roles[] = static::ROLE_DEFAULT;
        return array_unique($roles);
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function addGroup(GroupInterface $groups)
    {
        $this->groups[] = $groups;
        return $this;
    }

    public function removeGroup(GroupInterface $groups)
    {
        $this->groups->removeElement($groups);
    }

    public function getGroups()
    {
        return $this->groups;
    }

    public function getLastLoginString()
    {
        if($this->lastLogin){
            return $this->lastLogin->format('Y-m-d H:i:s');
        }
        else{
            return '';
        }
    }

    public function getCreatedAtString()
    {
        if($this->createdAt){
            return $this->createdAt->format('Y-m-d H:i:s');
        }
        else{
            return '';
        }
    }

    public function getExpiresAtString()
    {
        if($this->expiresAt){
            return $this->expiresAt->format('Y-m-d H:i:s');
        }
        else{
            return '';
        }
    }

    public function getPasswordRequestedAtString()
    {
        if($this->passwordRequestedAt){
            return $this->passwordRequestedAt->format('Y-m-d H:i:s');
        }
        else{
            return '';
        }
    }

    public function getCredentialsExpireAtString()
    {
        if($this->credentialsExpireAt){
            return $this->credentialsExpireAt->format('Y-m-d H:i:s');
        }
        else{
            return '';
        }
    }

}
