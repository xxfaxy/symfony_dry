<?php
namespace Xxfaxy\UserBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Xxfaxy\UserBundle\Entity\Group;
use Xxfaxy\UserBundle\Form\GroupType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForNewType;
use Xxfaxy\DryBundle\Form\Type\SubmitGroupForEditType;
use Xxfaxy\DryBundle\Entity\GroupPermission;

class GroupController extends Controller
{
    public function indexAction()
    {
        $twigData = array();
        $function = $this->get('service.common.function');
        $twigData['rolesArray'] = $function->getChoice('role');
        $em = $this->getDoctrine()->getManager();
        $groups = $em->getRepository('XxfaxyUserBundle:Group')->findAll();
        $twigData['groups'] = $groups;
        return $this->render('XxfaxyUserBundle:group:index.html.twig', $twigData);
    }

    public function newAction(Request $request)
    {
        $group = new Group();
        $form = $this->createForm('Xxfaxy\UserBundle\Form\GroupType', $group);
        $form->add('SubmitGroup', SubmitGroupForNewType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $form->handleRequest($request);
        /*if($form->isSubmitted() && $form->isValid()){*/
        if($form->isSubmitted()){
            $function = $this->get('service.common.function');
            $em = $this->getDoctrine()->getManager();
            $group->setRoles(array($request->request->get('roles')));
            $em->persist($group);
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            $post = $function->getFormData($request->request->all());
            $SubmitGroup = $post['SubmitGroup'];
            if(isset($SubmitGroup['submit'])){
                return $this->redirectToRoute('group_new');
            }
            else if(isset($SubmitGroup['submitAndShow'])){
                return $this->redirectToRoute('group_show', array('id' => $group->getId()));
            }
            else if(isset($SubmitGroup['submitAndList'])){
                return $this->redirectToRoute('group_index');
            }
            else if(isset($SubmitGroup['submitAndEdit'])){
                return $this->redirectToRoute('group_edit', array('id' => $group->getId()));
            }
        }
        return $this->render('XxfaxyUserBundle:group:new.html.twig', array(
            'group' => $group,
            'form' => $form->createView(),
        ));
    }

    public function showAction(Group $group)
    {
        $twigData = array();
        $function = $this->get('service.common.function');
        $twigData['rolesArray'] = $function->getChoice('role');
        $twigData['group'] = $group;
        return $this->render('XxfaxyUserBundle:group:show.html.twig', $twigData);
    }

    public function editAction(Request $request, Group $group)
    {
        $editForm = $this->createForm('Xxfaxy\UserBundle\Form\GroupType', $group);
        $editForm->add('SubmitGroup', SubmitGroupForEditType::class, array('label' => ' ', 'label_attr' => array('class' => 'control-label col-sm-2'), 'attr' => array('class' => 'form-control'), 'mapped' => false));
        $editForm->handleRequest($request);
        /*if($editForm->isSubmitted() && $editForm->isValid()){*/
        if($editForm->isSubmitted()){
            $function = $this->get('service.common.function');
            $em = $this->getDoctrine()->getManager();
            $group->setRoles(array($request->request->get('roles')));
            $em->persist($group);
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            $post = $function->getFormData($request->request->all());
            $SubmitGroup = $post['SubmitGroup'];
            if(isset($SubmitGroup['submit'])){
                return $this->redirectToRoute('group_edit', array('id' => $group->getId()));
            }
            else if(isset($SubmitGroup['submitAndShow'])){
                return $this->redirectToRoute('group_show', array('id' => $group->getId()));
            }
            else if(isset($SubmitGroup['submitAndList'])){
                return $this->redirectToRoute('group_index');
            }
            else if(isset($SubmitGroup['submitAndNew'])){
                return $this->redirectToRoute('group_new');
            }
        }
        return $this->render('XxfaxyUserBundle:group:edit.html.twig', array(
            'group' => $group,
            'edit_form' => $editForm->createView(),
        ));
    }

    public function deleteAction(Request $request, Group $group)
    {
        $form = $this->createDeleteForm($group);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($group);
            $em->flush();
        }
        return $this->redirectToRoute('group_index');
    }

    private function createDeleteForm(Group $group)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('group_delete', array('id' => $group->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function permissionAction(Request $request, Group $group, $id)
    {
        $twigData = array();
        $twigData['group'] = $group;
        $function = $this->get('service.common.function');
        $permission = $this->get('service.permission');
        $em = $this->getDoctrine()->getManager();
        $willDelete = $em->getRepository('XxfaxyDryBundle:GroupPermission')->findByDryGroup($id);
        $hasPermission = array();
        foreach($willDelete as $v){
            $hasPermission[] = $v->getDryRouteName();
        }
        $twigData['hasPermission'] = $hasPermission;
        if($request->isMethod('POST')){
            $willAsignPermission = $request->request->get('permission');
            foreach($willDelete as $v){
                $em->remove($v);
            }
            $user = $this->get('security.context')->getToken()->getUser();
            foreach($willAsignPermission as $v){
                $entity = new GroupPermission();
                $entity->setDryGroup($group);
                $entity->setDryRouteName($v);
                $entity->setDryNote($user->getUsername());
                $em->persist($entity);
            }
            $em->flush();
            $this->addFlash('message_success', $function->translation('operationSuccess'));
            return $this->redirectToRoute('group_permission', array('id' => $id));
        }
        $twigData['permission'] = $permission->getPermission(true);
        $twigData['elsePermission'] = $permission->getPermission(false);
        return $this->render('XxfaxyUserBundle:group:permission.html.twig', $twigData);
    }

}
