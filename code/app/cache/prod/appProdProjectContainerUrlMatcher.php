<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/adminx')) {
            if (0 === strpos($pathinfo, '/adminx/user')) {
                // user_index
                if (rtrim($pathinfo, '/') === '/adminx/user') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_user_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'user_index');
                    }

                    return array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\UserController::indexAction',  '_route' => 'user_index',);
                }
                not_user_index:

                // user_show
                if (preg_match('#^/adminx/user/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_user_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_show')), array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\UserController::showAction',));
                }
                not_user_show:

                // user_new
                if ($pathinfo === '/adminx/user/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_user_new;
                    }

                    return array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\UserController::newAction',  '_route' => 'user_new',);
                }
                not_user_new:

                // user_edit
                if (preg_match('#^/adminx/user/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_user_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_edit')), array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\UserController::editAction',));
                }
                not_user_edit:

                // user_delete
                if (preg_match('#^/adminx/user/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_user_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_delete')), array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\UserController::deleteAction',));
                }
                not_user_delete:

                // user_update_password
                if ($pathinfo === '/adminx/user/update/password') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_user_update_password;
                    }

                    return array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\UserController::updatePasswordAction',  '_route' => 'user_update_password',);
                }
                not_user_update_password:

                // user_login
                if (preg_match('#^/adminx/user/(?P<id>[^/]++)/login$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_user_login;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_login')), array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\UserController::loginAction',));
                }
                not_user_login:

            }

            if (0 === strpos($pathinfo, '/adminx/group')) {
                // group_index
                if (rtrim($pathinfo, '/') === '/adminx/group') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_group_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'group_index');
                    }

                    return array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\GroupController::indexAction',  '_route' => 'group_index',);
                }
                not_group_index:

                // group_show
                if (preg_match('#^/adminx/group/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_group_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'group_show')), array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\GroupController::showAction',));
                }
                not_group_show:

                // group_new
                if ($pathinfo === '/adminx/group/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_group_new;
                    }

                    return array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\GroupController::newAction',  '_route' => 'group_new',);
                }
                not_group_new:

                // group_edit
                if (preg_match('#^/adminx/group/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_group_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'group_edit')), array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\GroupController::editAction',));
                }
                not_group_edit:

                // group_delete
                if (preg_match('#^/adminx/group/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_group_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'group_delete')), array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\GroupController::deleteAction',));
                }
                not_group_delete:

                // group_permission
                if (preg_match('#^/adminx/group/(?P<id>[^/]++)/permission$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_group_permission;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'group_permission')), array (  '_controller' => 'Xxfaxy\\UserBundle\\Controller\\GroupController::permissionAction',));
                }
                not_group_permission:

            }

            if (0 === strpos($pathinfo, '/adminx/setting')) {
                // setting_index
                if (rtrim($pathinfo, '/') === '/adminx/setting') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_setting_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'setting_index');
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\SettingController::indexAction',  '_route' => 'setting_index',);
                }
                not_setting_index:

                // setting_show
                if (preg_match('#^/adminx/setting/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_setting_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'setting_show')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\SettingController::showAction',));
                }
                not_setting_show:

                // setting_new
                if ($pathinfo === '/adminx/setting/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_setting_new;
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\SettingController::newAction',  '_route' => 'setting_new',);
                }
                not_setting_new:

                // setting_edit
                if (preg_match('#^/adminx/setting/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_setting_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'setting_edit')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\SettingController::editAction',));
                }
                not_setting_edit:

                // setting_delete
                if (preg_match('#^/adminx/setting/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_setting_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'setting_delete')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\SettingController::deleteAction',));
                }
                not_setting_delete:

            }

            if (0 === strpos($pathinfo, '/adminx/choice')) {
                // choice_index
                if (rtrim($pathinfo, '/') === '/adminx/choice') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_choice_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'choice_index');
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\ChoiceController::indexAction',  '_route' => 'choice_index',);
                }
                not_choice_index:

                // choice_show
                if (preg_match('#^/adminx/choice/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_choice_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'choice_show')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\ChoiceController::showAction',));
                }
                not_choice_show:

                // choice_new
                if ($pathinfo === '/adminx/choice/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_choice_new;
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\ChoiceController::newAction',  '_route' => 'choice_new',);
                }
                not_choice_new:

                // choice_edit
                if (preg_match('#^/adminx/choice/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_choice_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'choice_edit')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\ChoiceController::editAction',));
                }
                not_choice_edit:

                // choice_delete
                if (preg_match('#^/adminx/choice/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_choice_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'choice_delete')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\ChoiceController::deleteAction',));
                }
                not_choice_delete:

            }

            if (0 === strpos($pathinfo, '/adminx/menu')) {
                // menu_index
                if (rtrim($pathinfo, '/') === '/adminx/menu') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_menu_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'menu_index');
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\MenuController::indexAction',  '_route' => 'menu_index',);
                }
                not_menu_index:

                // menu_show
                if (preg_match('#^/adminx/menu/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_menu_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'menu_show')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\MenuController::showAction',));
                }
                not_menu_show:

                // menu_new
                if ($pathinfo === '/adminx/menu/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_menu_new;
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\MenuController::newAction',  '_route' => 'menu_new',);
                }
                not_menu_new:

                // menu_edit
                if (preg_match('#^/adminx/menu/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_menu_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'menu_edit')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\MenuController::editAction',));
                }
                not_menu_edit:

                // menu_delete
                if (preg_match('#^/adminx/menu/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_menu_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'menu_delete')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\MenuController::deleteAction',));
                }
                not_menu_delete:

                // menu_update_data_sort
                if ($pathinfo === '/adminx/menu/update/data/sort') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_menu_update_data_sort;
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\MenuController::updateDataSortAction',  '_route' => 'menu_update_data_sort',);
                }
                not_menu_update_data_sort:

            }

            if (0 === strpos($pathinfo, '/adminx/grouppermission')) {
                // grouppermission_index
                if (rtrim($pathinfo, '/') === '/adminx/grouppermission') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grouppermission_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'grouppermission_index');
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\GroupPermissionController::indexAction',  '_route' => 'grouppermission_index',);
                }
                not_grouppermission_index:

                // grouppermission_show
                if (preg_match('#^/adminx/grouppermission/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_grouppermission_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grouppermission_show')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\GroupPermissionController::showAction',));
                }
                not_grouppermission_show:

                // grouppermission_new
                if ($pathinfo === '/adminx/grouppermission/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_grouppermission_new;
                    }

                    return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\GroupPermissionController::newAction',  '_route' => 'grouppermission_new',);
                }
                not_grouppermission_new:

                // grouppermission_edit
                if (preg_match('#^/adminx/grouppermission/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_grouppermission_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grouppermission_edit')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\GroupPermissionController::editAction',));
                }
                not_grouppermission_edit:

                // grouppermission_delete
                if (preg_match('#^/adminx/grouppermission/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_grouppermission_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grouppermission_delete')), array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\GroupPermissionController::deleteAction',));
                }
                not_grouppermission_delete:

            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        // home
        if (rtrim($pathinfo, '/') === '') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_home;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home');
            }

            return array (  '_controller' => 'Xxfaxy\\DryBundle\\Controller\\SystemController::homeAction',  '_route' => 'home',);
        }
        not_home:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
