-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.1.20
-- Generation Time: 2017-10-05 23:13:53
-- 服务器版本： 5.7.13
-- PHP Version: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `symfony_dry`
--

-- --------------------------------------------------------

--
-- 表的结构 `choice`
--

CREATE TABLE `choice` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `dryName` varchar(255) DEFAULT NULL COMMENT '名称',
  `dryKey` varchar(255) NOT NULL COMMENT '键',
  `dryValue` text COMMENT '值',
  `drySort` int(11) NOT NULL COMMENT '排序',
  `dryAddtime` datetime NOT NULL COMMENT '添加时间',
  `dryStatus` int(11) NOT NULL COMMENT '状态(0禁用 1启用)',
  `dryNote` varchar(255) DEFAULT NULL COMMENT '备注',
  `dryParent` int(11) DEFAULT NULL COMMENT 'ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选项';

--
-- 转存表中的数据 `choice`
--

INSERT INTO `choice` (`id`, `dryName`, `dryKey`, `dryValue`, `drySort`, `dryAddtime`, `dryStatus`, `dryNote`, `dryParent`) VALUES
(1, '状态', 'status', '[\r\n    {"status":"1","key":"1","value":"启用","note":""},\r\n    {"status":"1","key":"0","value":"禁用","note":""}\r\n]', 1, '2017-08-31 15:47:25', 1, NULL, NULL),
(2, '链接打开方式', 'target', '[\r\n    {"status":"1","key":"_parent","value":"父窗口打开","note":""},\r\n    {"status":"1","key":"_blank","value":"新窗口打开","note":""}\r\n]', 2, '2017-08-31 16:46:40', 1, NULL, NULL),
(3, '设置分组', 'setting_group', '[\r\n    {"status":"1","key":"system","value":"系统","note":""},\r\n    {"status":"1","key":"common","value":"通用","note":""}\r\n]', 3, '2017-09-01 14:07:12', 1, NULL, NULL),
(4, '是或否', 'yes_or_no', '[\r\n    {"status":"1","key":"yes","value":"是","note":""},\r\n    {"status":"1","key":"no","value":"否","note":""}\r\n]', 4, '2017-09-01 14:08:17', 1, NULL, NULL),
(5, '用户组', 'role', '[\r\n    {"status":"1","key":"ROLE_SUPER_ADMIN","value":"ROLE_SUPER_ADMIN","note":"超管"},\r\n    {"status":"1","key":"ROLE_ADMIN","value":"ROLE_ADMIN","note":"管理员"},\r\n    {"status":"1","key":"ROLE_USER","value":"ROLE_USER","note":"用户"}\r\n]', 5, '2017-09-01 15:53:23', 1, NULL, NULL),
(6, '是否正确', 'is_correct', '[\r\n    {"status":"1","key":"1","value":"正确","note":""},\r\n    {"status":"1","key":"0","value":"错误","note":""}\r\n]', 6, '2017-09-04 14:16:32', 1, NULL, NULL),
(7, '表单类型', 'choice_config_form_type', '[\r\n    {"status":"1","key":"horizontal_radio","value":"水平单选","note":""},\r\n    {"status":"1","key":"horizontal_checkbox","value":"水平多选","note":""},\r\n    {"status":"1","key":"vertical_radio","value":"垂直单选","note":""},\r\n    {"status":"1","key":"vertical_checkbox","value":"垂直多选","note":""},\r\n    {"status":"1","key":"select","value":"单选下拉","note":""},\r\n    {"status":"1","key":"multiple_select","value":"多选下拉","note":""}\r\n]', 7, '2017-09-12 15:08:04', 1, NULL, NULL),
(8, '操作符', 'search_operator', '[\r\n    {"status":"1","key":"<","value":"小于","note":""},\r\n    {"status":"1","key":"<=","value":"小于等于","note":""},\r\n    {"status":"1","key":"=","value":"等于","note":""},\r\n    {"status":"1","key":">=","value":"大于等于","note":""},\r\n    {"status":"1","key":">","value":"大于","note":""},\r\n    {"status":"1","key":"<>","value":"不等于","note":""},\r\n    {"status":"1","key":"like","value":"like","note":""},\r\n    {"status":"1","key":"not like","value":"not like","note":""},\r\n    {"status":"1","key":"between and","value":"between and","note":""},\r\n    {"status":"1","key":"contain","value":"contain","note":""},\r\n    {"status":"1","key":"not contain","value":"not contain","note":""},\r\n    {"status":"1","key":"match word","value":"match word","note":""},\r\n    {"status":"1","key":"in","value":"in","note":""},\r\n    {"status":"1","key":"not in","value":"not in","note":""}\r\n]', 8, '2017-09-13 19:16:42', 1, NULL, NULL),
(9, '表单类型', 'search_form_type', '[\r\n    {"status":"1","key":"input","value":"输入框","note":""},\r\n    {"status":"1","key":"radio","value":"单选","note":""},\r\n    {"status":"1","key":"checkbox","value":"多选","note":""},\r\n    {"status":"1","key":"select","value":"下拉","note":""},\r\n    {"status":"1","key":"date","value":"日期","note":""},\r\n    {"status":"1","key":"datetime","value":"日期时间","note":""}\r\n]', 9, '2017-09-13 19:19:39', 1, NULL, NULL),
(10, '性别', 'sex_select', '[\r\n    {"status":"1","key":"male","value":"男","note":""},\r\n    {"status":"1","key":"female","value":"女","note":""}\r\n]', 10, '2017-09-19 13:54:06', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dry_choice_config`
--

CREATE TABLE `dry_choice_config` (
  `id` int(11) UNSIGNED NOT NULL,
  `dry_form_type` varchar(32) NOT NULL,
  `dry_form_text` varchar(64) NOT NULL,
  `dry_form_name` varchar(32) NOT NULL,
  `dry_key` varchar(64) NOT NULL,
  `dry_choice` varchar(64) NOT NULL,
  `dry_choice_default` varchar(64) NOT NULL,
  `dry_note` varchar(64) DEFAULT NULL,
  `dry_sort` int(11) NOT NULL,
  `dry_add_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dry_choice_config`
--

INSERT INTO `dry_choice_config` (`id`, `dry_form_type`, `dry_form_text`, `dry_form_name`, `dry_key`, `dry_choice`, `dry_choice_default`, `dry_note`, `dry_sort`, `dry_add_time`) VALUES
(1, 'horizontal_radio', '状态', 'dry_status', 'status', 'status', '1', '', 1, '2017-07-24 22:33:09'),
(5, 'horizontal_radio', '表单类型', 'dry_form_type', 'choice_config_form_type', 'choice_config_form_type', 'horizontal_radio', '表单类型', 5, '2017-07-25 10:42:48'),
(7, 'horizontal_radio', '自动', 'dry_auto', 'auto_yes_or_no', 'yes_or_no', 'yes', '', 7, '2017-09-06 10:51:51'),
(8, 'select', '语言', 'dry_language', 'language', '@language_for_permission', '', '', 8, '2017-09-06 11:28:14'),
(9, 'horizontal_radio', '拥有', 'dry_own', 'own_yes_or_no', 'yes_or_no', 'yes', '', 6, '2017-09-13 13:47:00'),
(10, 'horizontal_radio', '单独一行', 'dry_single_row', 'single_row_yes_or_no', 'yes_or_no', 'yes', '', 9, '2017-09-13 16:34:51'),
(11, 'select', '操作符', 'dry_operator', 'search_operator', 'search_operator', '=', '', 10, '2017-09-13 19:13:31'),
(12, 'horizontal_radio', '表单类型', 'dry_form_type', 'search_form_type', 'search_form_type', 'input', '', 11, '2017-09-13 19:21:13'),
(13, 'horizontal_radio', '性别', 'dry_sex', 'sex', 'sex_select', '', '', 12, '2017-09-19 13:47:20');

-- --------------------------------------------------------

--
-- 表的结构 `dry_language`
--

CREATE TABLE `dry_language` (
  `id` int(10) UNSIGNED NOT NULL,
  `dry_group` varchar(64) DEFAULT NULL,
  `dry_key` varchar(255) DEFAULT NULL,
  `dry_en_us` varchar(255) DEFAULT NULL,
  `dry_zh_cn` varchar(255) DEFAULT NULL,
  `dry_note` varchar(255) DEFAULT NULL,
  `dry_sort` int(11) DEFAULT NULL,
  `dry_add_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dry_language`
--

INSERT INTO `dry_language` (`id`, `dry_group`, `dry_key`, `dry_en_us`, `dry_zh_cn`, `dry_note`, `dry_sort`, `dry_add_time`) VALUES
(1, '通用按钮', 'action', 'Action', '操作', '', 100001, '2017-09-04 15:53:33'),
(2, '通用按钮', 'new', 'New', '创建', '', 100002, '2017-09-04 16:19:39'),
(3, '系统消息', 'operationSuccess', 'Success', '操作成功', '', 300002, '2017-09-05 17:18:04'),
(4, '通用按钮', 'list', 'List', '列表', '', 100006, '2017-09-05 17:23:08'),
(5, 'crud拓展按钮', 'languageGenerateTranslationFile', 'Generate translation file', '语言生成翻译文件', '', 400002, '2017-09-05 17:31:12'),
(6, '通用按钮', 'show', 'Show', '查看', '', 100005, '2017-09-05 17:32:33'),
(7, '通用按钮', 'edit', 'Edit', '编辑', '', 100004, '2017-09-05 17:33:12'),
(8, '系统消息', 'systemMessage', 'System message', '系统消息', '', 300001, '2017-09-05 17:34:05'),
(9, '通用按钮', 'delete', 'Delete', '删除', '', 100003, '2017-09-05 17:36:45'),
(10, '通用按钮', 'detail', 'Detail', '详情', '', 100007, '2017-09-05 17:37:23'),
(11, '提交按钮', 'submit', 'Submit', '提交', '', 200001, '2017-09-05 17:38:03'),
(12, '提交按钮', 'submitAndShow', 'Submit and show', '提交->查看', '', 200004, '2017-09-05 17:42:44'),
(13, '提交按钮', 'submitAndList', 'Submit and list', '提交->列表', '', 200005, '2017-09-05 17:43:20'),
(14, '提交按钮', 'submitAndEdit', 'Submit and edit', '提交->编辑', '', 200003, '2017-09-05 17:44:36'),
(15, '提交按钮', 'submitAndNew', 'Submit and new', '提交->创建', '', 200002, '2017-09-05 17:45:37'),
(16, '全局', 'username', 'Username', '用户名', '', 600005, '2017-09-05 17:46:12'),
(17, '全局', 'password', 'Password', '密码', '', 600006, '2017-09-05 17:46:29'),
(18, '全局', 'login', 'Login', '登录', '', 600004, '2017-09-05 17:46:44'),
(19, '全局', 'loginTitle', 'OA+', 'OA+', '', 600002, '2017-09-05 17:47:09'),
(20, 'crud', 'group', 'Group', '用户组', '', 700001, '2017-09-05 17:47:27'),
(21, '全局', 'id', 'ID', 'ID', '', 600003, '2017-09-05 17:47:39'),
(22, '全局', 'shortName', 'DRY', 'DRY', '', 600001, '2017-09-05 17:47:58'),
(23, '系统消息', 'operationFail', 'Fail', '操作失败', '', 300003, '2017-09-05 17:48:26'),
(24, '系统消息', 'Invalid credentials.', 'Invalid credentials', '用户名或密码不正确', '', 300004, '2017-09-05 17:49:02'),
(25, '系统消息', 'Invalid CSRF token.', 'Invalid CSRF token', 'CSRF错误', '', 300005, '2017-09-05 17:49:31'),
(26, '全局', 'updatePassword', 'Update password', '更新密码', '', 600007, '2017-09-05 17:53:55'),
(27, '全局', 'permissionAsign', 'Permission asign', '权限分配', '', 600013, '2017-09-05 17:54:41'),
(28, 'crud拓展按钮', 'menuUpdateDataSort', 'Update menu sort', '菜单更新数据排序', '', 400001, '2017-09-05 17:56:08'),
(29, 'crud', 'user', 'User', '用户', '', 700002, '2017-09-06 09:30:55'),
(30, 'crud', 'menu', 'Menu', '菜单', '', 700003, '2017-09-06 09:31:21'),
(31, 'crud', 'groupPermission', 'Group permission', '用户组权限', '', 700004, '2017-09-06 09:32:06'),
(32, 'crud', 'choice', 'Choice', '选项', '', 700005, '2017-09-06 09:32:36'),
(33, 'crud', 'setting', 'Setting', '设置', '', 700006, '2017-09-06 09:32:56'),
(34, 'crud', 'language', 'Language', '语言', '', 700007, '2017-09-06 09:33:16'),
(35, 'crud', 'permission', 'Permission', '权限', '', 700008, '2017-09-06 13:21:19'),
(36, '全局', 'selectAll', 'Select all', '全选', '', 600008, '2017-09-06 14:09:18'),
(37, '全局', 'selectTheInverse', 'Select the inverse', '反选', '', 600009, '2017-09-06 14:10:00'),
(38, '全局', 'name', 'Name', '名称', '', 600010, '2017-09-06 14:11:55'),
(39, 'crud拓展按钮', 'groupPermissionAsign', 'Group permission asign', '用户组权限分配', '', 400003, '2017-09-06 14:46:45'),
(40, '全局', 'logout', 'Logout', '退出', '', 600011, '2017-09-06 17:12:45'),
(41, '密码相关', 'oldPassword', 'Old password', '原密码', '', 700101, '2017-09-07 15:32:34'),
(42, '密码相关', 'newPassword', 'New Password', '新密码', '', 700102, '2017-09-07 15:33:29'),
(43, '密码相关', 'newRePassword', 'Confirm password', '确认新密码', '', 700103, '2017-09-07 15:34:50'),
(44, '密码相关', 'oldPasswordIncorrect', 'The original password is incorrect', '原密码不正确', '', 700104, '2017-09-07 15:36:03'),
(45, '密码相关', 'newPassword9characters', 'New password at least 9 characters', '新密码至少9个字符', '', 700105, '2017-09-07 15:37:11'),
(46, '密码相关', 'passwordTwoTimeInconsistent', 'The password entered for the two time is inconsistent', '两次输入的密码不一致', '', 700106, '2017-09-07 15:39:41'),
(47, 'Group', 'Group.id', '', 'ID', '', 700201, '2017-09-07 15:49:08'),
(48, 'Group', 'Group.name', '', '名称', '', 700202, '2017-09-07 15:50:42'),
(49, 'Group', 'Group.roles', '', '角色', '', 700203, '2017-09-07 15:51:25'),
(50, 'User', 'User.id', 'ID', 'ID', '', 700301, '2017-09-07 15:57:14'),
(51, 'User', 'User.username', '', '用户名', '', 700302, '2017-09-07 15:57:55'),
(52, 'User', 'User.groups', '', '用户组', '', 700303, '2017-09-07 15:58:34'),
(53, 'User', 'User.email', '', '邮箱', '', 700304, '2017-09-07 15:58:56'),
(54, 'User', 'User.enabled', '', '是否可用', '', 700305, '2017-09-07 15:59:18'),
(55, 'User', 'User.lastLogin', '', '上次登录时间', '', 700306, '2017-09-07 15:59:39'),
(56, 'User', 'User.createdAt', '', '创建时间', '', 700307, '2017-09-07 16:00:05'),
(57, 'User', 'User.password', '', '密码', '', 700308, '2017-09-07 16:02:25'),
(58, 'User', 'User.usernameCanonical', '', '正式用户名', '', 700309, '2017-09-07 16:03:21'),
(59, 'User', 'User.emailCanonical', '', '正式邮箱', '', 700310, '2017-09-07 16:03:45'),
(60, 'User', 'User.salt', '', '盐', '', 700311, '2017-09-07 16:04:04'),
(61, 'User', 'User.locked', '', '是否锁定', '', 700312, '2017-09-07 16:04:53'),
(62, 'User', 'User.expired', '', '是否过期', '', 700313, '2017-09-07 16:05:15'),
(63, 'User', 'User.expiresAt', '', '过期时间', '', 700314, '2017-09-07 16:05:35'),
(64, 'User', 'User.confirmationToken', '', '确认令牌', '', 700315, '2017-09-07 16:05:56'),
(65, 'User', 'User.passwordRequestedAt', '', '密码请求时间', '', 700316, '2017-09-07 16:06:19'),
(66, 'User', 'User.roles', '', '角色', '', 700317, '2017-09-07 16:06:41'),
(67, 'User', 'User.credentialsExpired', '', '凭据是否过期', '', 700318, '2017-09-07 16:07:02'),
(68, 'User', 'User.credentialsExpireAt', '', '凭据过期时间', '', 700319, '2017-09-07 16:07:25'),
(69, 'Menu', 'Menu.id', '', 'ID', '', 700401, '2017-09-07 16:11:17'),
(70, 'Menu', 'Menu.dryParent', '', '父级', '', 700402, '2017-09-07 16:11:41'),
(71, 'Menu', 'Menu.dryGrade', '', '等级', '', 700403, '2017-09-07 16:12:04'),
(72, 'Menu', 'Menu.dryIcon', 'ICON', 'ICON', '', 700404, '2017-09-07 16:12:26'),
(73, 'Menu', 'Menu.dryName', '', '名称', '', 700405, '2017-09-07 16:12:46'),
(74, 'Menu', 'Menu.dryRouteName', '', '路由名', '', 700406, '2017-09-07 16:13:08'),
(75, 'Menu', 'Menu.dryUrl', 'URL', 'URL', '', 700407, '2017-09-07 16:13:30'),
(76, 'Menu', 'Menu.dryTarget', '', '打开方式', '', 700408, '2017-09-07 16:13:53'),
(77, 'Menu', 'Menu.dryStatus', '', '状态', '', 700409, '2017-09-07 16:14:15'),
(78, 'Menu', 'Menu.dryNote', '', '备注', '', 700410, '2017-09-07 16:14:35'),
(79, 'Menu', 'Menu.drySort', '', '排序', '', 700411, '2017-09-07 16:14:56'),
(80, 'Menu', 'Menu.dryAddtime', '', '添加时间', '', 700412, '2017-09-07 16:15:18'),
(81, 'Menu', 'Menu.dryDataSort', '', '数据排序', '', 700413, '2017-09-07 16:20:55'),
(82, 'GroupPermission', 'GroupPermission.id', 'ID', 'ID', '', 700501, '2017-09-07 16:21:56'),
(83, 'GroupPermission', 'GroupPermission.dryGroup', '', '用户组', '', 700502, '2017-09-07 16:22:19'),
(84, 'GroupPermission', 'GroupPermission.dryRouteName', '', '路由名称', '', 700503, '2017-09-07 16:22:38'),
(85, 'GroupPermission', 'GroupPermission.dryAddtime', '', '添加时间', '', 700504, '2017-09-07 16:23:02'),
(86, 'GroupPermission', 'GroupPermission.dryNote', '', '备注', '', 700505, '2017-09-07 16:23:21'),
(87, 'Choice', 'Choice.id', 'ID', 'ID', '', 700601, '2017-09-07 16:25:42'),
(88, 'Choice', 'Choice.dryParent', '', '上级', '', 700602, '2017-09-07 16:26:00'),
(89, 'Choice', 'Choice.dryName', '', '名称', '', 700603, '2017-09-07 16:26:20'),
(90, 'Choice', 'Choice.dryKey', '', '键', '', 700604, '2017-09-07 16:26:37'),
(91, 'Choice', 'Choice.dryValue', '', '值', '', 700605, '2017-09-07 16:26:54'),
(92, 'Choice', 'Choice.drySort', '', '排序', '', 700606, '2017-09-07 16:27:12'),
(93, 'Choice', 'Choice.dryAddtime', '', '添加时间', '', 700607, '2017-09-07 16:27:32'),
(94, 'Choice', 'Choice.dryStatus', '', '状态', '', 700608, '2017-09-07 16:27:54'),
(95, 'Choice', 'Choice.dryNote', '', '备注', '', 700609, '2017-09-07 16:28:13'),
(96, 'Setting', 'Setting.id', 'ID', 'ID', '', 700701, '2017-09-07 16:30:29'),
(97, 'Setting', 'Setting.dryGroup', '', '分组', '', 700702, '2017-09-07 16:31:00'),
(98, 'Setting', 'Setting.dryName', '', '名称', '', 700703, '2017-09-07 16:31:17'),
(99, 'Setting', 'Setting.dryValue', '', '值', '', 700704, '2017-09-07 16:31:32'),
(100, 'Setting', 'Setting.drySort', '', '排序', '', 700705, '2017-09-07 16:31:47'),
(101, 'Setting', 'Setting.dryAddtime', '', '添加时间', '', 700706, '2017-09-07 16:32:03'),
(102, 'Setting', 'Setting.dryNote', '', '备注', '', 700707, '2017-09-07 16:32:19'),
(103, 'Setting', 'Setting.dryUseDetail', '', '详情展示', '', 700708, '2017-09-07 16:32:43'),
(104, 'Setting', 'Setting.dryIsJsonString', '', 'json数据', '', 700709, '2017-09-07 16:33:03'),
(105, 'crud拓展按钮', 'userUpdatePassword', 'User update password', '用户更新密码', '', 400004, '2017-09-07 16:53:07'),
(106, 'crud拓展按钮', 'userSimulateLogin', 'User simulate login', '用户模拟登录', '', 400005, '2017-09-07 16:58:06'),
(107, 'Setting', 'Setting.jsonFormatCheck', 'Json format check', 'json格式检查', '', 700710, '2017-09-08 09:57:56'),
(108, 'crud', 'choiceConfig', 'Choice config', '选项配置', '', 700009, '2017-09-12 15:49:17'),
(109, 'crud', 'tableKeyValue', 'Table key value', '表键值', '', 700010, '2017-09-12 15:50:04'),
(110, '全局', 'resetUserPassword', 'Reset user password', '重置用户密码', '', 600012, '2017-09-14 14:13:09'),
(111, '密码相关', 'usernameIncorrect', 'Username incorrect', '用户名不正确', '', 700107, '2017-09-14 14:41:55'),
(112, 'Search', 'Search.dryGroup', '', '组', '', 700802, '2017-09-14 10:59:45'),
(113, 'Search', 'Search.dryName', '', '名称', '', 700803, '2017-09-14 11:00:49'),
(114, 'Search', 'Search.dryField', '', '字段', '', 700804, '2017-09-14 11:01:18'),
(115, 'Search', 'Search.dryOperator', '', '操作符', '', 700805, '2017-09-14 11:01:51'),
(116, 'Search', 'Search.dryFormType', '', '表单类型', '', 700806, '2017-09-14 11:02:40'),
(117, 'Search', 'Search.dryDefault', '', '默认值', '', 700808, '2017-09-14 11:03:02'),
(118, 'Search', 'Search.drySingleRow', '', '单独一行', '', 700809, '2017-09-14 11:03:23'),
(119, 'Search', 'Search.dryStatus', '', '状态', '', 700807, '2017-09-14 11:03:45'),
(120, 'Search', 'Search.drySort', '', '排序', '', 700810, '2017-09-14 11:04:12'),
(121, 'Search', 'Search.dryNote', '', '备注', '', 700811, '2017-09-14 11:04:41'),
(122, 'Search', 'Search.dryAddTime', '', '添加时间', '', 700812, '2017-09-14 11:05:08'),
(123, 'Search', 'Search.id', 'ID', 'ID', '', 700801, '2017-09-14 10:58:44'),
(124, 'crud', 'searchConfig', 'Search config', '搜索配置', '', 700011, '2017-09-14 16:53:23'),
(125, 'crud', 'userPermission', 'User Permission', '用户权限', '', 700012, '2017-09-14 16:54:03');

-- --------------------------------------------------------

--
-- 表的结构 `dry_permission`
--

CREATE TABLE `dry_permission` (
  `id` int(10) UNSIGNED NOT NULL,
  `dry_route` varchar(64) DEFAULT NULL,
  `dry_menu_route` varchar(64) DEFAULT NULL,
  `dry_auto` char(3) DEFAULT NULL,
  `dry_language` varchar(64) DEFAULT NULL,
  `dry_note` varchar(64) DEFAULT NULL,
  `dry_sort` int(11) DEFAULT NULL,
  `dry_add_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dry_permission`
--

INSERT INTO `dry_permission` (`id`, `dry_route`, `dry_menu_route`, `dry_auto`, `dry_language`, `dry_note`, `dry_sort`, `dry_add_time`) VALUES
(1, 'group', '', 'yes', 'group', '', 1, '2017-09-06 13:17:40'),
(2, 'user', '', 'yes', 'user', '', 2, '2017-09-06 13:18:19'),
(3, 'menu', '', 'yes', 'menu', '', 3, '2017-09-06 13:18:36'),
(4, 'grouppermission', '', 'yes', 'groupPermission', '', 4, '2017-09-06 13:19:16'),
(5, 'choice', '', 'yes', 'choice', '', 5, '2017-09-06 13:19:31'),
(6, 'setting', '', 'yes', 'setting', '', 6, '2017-09-06 13:19:46'),
(7, 'language', '', 'yes', 'language', '', 7, '2017-09-06 13:20:30'),
(8, 'permission', '', 'yes', 'permission', '', 8, '2017-09-06 13:21:57'),
(9, 'group_permission', 'group_index', 'no', 'groupPermissionAsign', '', 1001, '2017-09-06 14:43:58'),
(10, 'language_make', '', 'no', 'languageGenerateTranslationFile', '', 1002, '2017-09-06 14:53:30'),
(11, 'user_update_password', '', 'no', 'userUpdatePassword', '', 1003, '2017-09-07 16:53:43'),
(12, 'user_login', '', 'no', 'userSimulateLogin', '', 1004, '2017-09-07 16:59:09'),
(13, 'menu_update_data_sort', '', 'no', 'menuUpdateDataSort', '', 1005, '2017-09-07 17:06:03'),
(14, 'choiceconfig', '', 'yes', 'choiceConfig', '', 9, '2017-09-12 15:51:19'),
(15, 'tablekeyvalue', '', 'yes', 'tableKeyValue', '', 10, '2017-09-12 15:51:43');

-- --------------------------------------------------------

--
-- 表的结构 `dry_search`
--

CREATE TABLE `dry_search` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `dry_group` varchar(32) NOT NULL COMMENT '组',
  `dry_name` varchar(32) NOT NULL COMMENT '名称',
  `dry_field` varchar(32) NOT NULL COMMENT '字段',
  `dry_operator` varchar(64) DEFAULT NULL COMMENT '操作符',
  `dry_form_type` varchar(32) DEFAULT NULL COMMENT '表单类型',
  `dry_default` varchar(128) DEFAULT NULL COMMENT '默认值',
  `dry_single_row` char(3) DEFAULT NULL COMMENT '单独一行',
  `dry_status` tinyint(4) DEFAULT NULL COMMENT '状态(0禁用 1启用)',
  `dry_sort` int(11) DEFAULT NULL COMMENT '排序',
  `dry_note` varchar(255) DEFAULT NULL COMMENT '备注',
  `dry_add_time` datetime DEFAULT NULL COMMENT '添加时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='搜索';

--
-- 转存表中的数据 `dry_search`
--

INSERT INTO `dry_search` (`id`, `dry_group`, `dry_name`, `dry_field`, `dry_operator`, `dry_form_type`, `dry_default`, `dry_single_row`, `dry_status`, `dry_sort`, `dry_note`, `dry_add_time`) VALUES
(1, 'user', '注册时间', 'created_at', 'between and', 'datetime', '', 'no', 1, 3, '', '2017-09-14 10:11:20'),
(2, 'user', '状态', 'enabled', '=', 'select', '1', 'yes', 1, 2, '', '2017-09-14 14:58:35'),
(3, 'user', '用户名', 'username', 'like', 'input', '', 'no', 1, 1, '', '2017-09-14 17:17:57');

-- --------------------------------------------------------

--
-- 表的结构 `dry_table_key_value`
--

CREATE TABLE `dry_table_key_value` (
  `id` int(10) UNSIGNED NOT NULL,
  `dry_alias` varchar(64) DEFAULT NULL,
  `dry_table` varchar(64) DEFAULT NULL,
  `dry_key` varchar(64) DEFAULT NULL,
  `dry_value` varchar(64) DEFAULT NULL,
  `dry_order` varchar(128) DEFAULT NULL,
  `dry_sql` text,
  `dry_note` varchar(64) DEFAULT NULL,
  `dry_sort` int(11) DEFAULT NULL,
  `dry_add_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dry_table_key_value`
--

INSERT INTO `dry_table_key_value` (`id`, `dry_alias`, `dry_table`, `dry_key`, `dry_value`, `dry_order`, `dry_sql`, `dry_note`, `dry_sort`, `dry_add_time`) VALUES
(1, 'language_for_permission', 'dry_language', 'dry_key', 'dry_key', 'dry_sort asc', '', '', 1, '2017-09-12 13:25:48');

-- --------------------------------------------------------

--
-- 表的结构 `dry_user_permission`
--

CREATE TABLE `dry_user_permission` (
  `id` int(10) UNSIGNED NOT NULL,
  `dry_username` varchar(32) DEFAULT NULL,
  `dry_own` varchar(3) DEFAULT NULL,
  `dry_route` varchar(64) DEFAULT NULL,
  `dry_status` tinyint(4) DEFAULT NULL,
  `dry_note` varchar(128) DEFAULT NULL,
  `dry_sort` int(11) DEFAULT NULL,
  `dry_add_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dry_user_permission`
--

INSERT INTO `dry_user_permission` (`id`, `dry_username`, `dry_own`, `dry_route`, `dry_status`, `dry_note`, `dry_sort`, `dry_add_time`) VALUES
(1, 'user', 'yes', 'menu_update_data_sort', 1, '', 1, '2017-09-13 13:55:35');

-- --------------------------------------------------------

--
-- 表的结构 `fos_group`
--

CREATE TABLE `fos_group` (
  `id` int(11) NOT NULL,
  `name` varchar(180) NOT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `fos_group`
--

INSERT INTO `fos_group` (`id`, `name`, `roles`) VALUES
(1, '超管', 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}'),
(2, '管理员', 'a:1:{i:0;s:10:"ROLE_ADMIN";}'),
(3, '用户', 'a:1:{i:0;s:9:"ROLE_USER";}');

-- --------------------------------------------------------

--
-- 表的结构 `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) NOT NULL,
  `username_canonical` varchar(180) NOT NULL,
  `email` varchar(180) NOT NULL,
  `email_canonical` varchar(180) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `created_at`) VALUES
(1, 'super', 'super', 'super@qq.com', 'super@qq.com', 1, NULL, '$2y$13$sD9TrCOwwVGH/yXP6.6QZenaVVpC7eBp5F1FNnEmEET6o3HjXrngm', '2017-09-27 16:39:36', NULL, NULL, 'a:0:{}', '2016-04-04 20:48:01'),
(2, 'admin', 'admin', 'admin@qq.com', 'admin@qq.com', 1, NULL, '$2y$13$RAEGjq4fKtDtkti4VMkNbOBhh/t4DJzST95mqy6/4vnpQ8myVxv/u', '2017-09-19 12:02:35', NULL, NULL, 'a:0:{}', '2016-04-17 15:09:21'),
(3, 'user', 'user', 'user@qq.com', 'user@qq.com', 1, NULL, '$2y$13$Gt0o3jKdVQhtqig1tzdh8uqCVIjkfcOxPGlzE7YAcjP9.q0v52N.m', '2017-09-14 15:46:35', NULL, NULL, 'a:0:{}', '2016-12-03 21:11:48');

-- --------------------------------------------------------

--
-- 表的结构 `fos_user_group`
--

CREATE TABLE `fos_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `fos_user_group`
--

INSERT INTO `fos_user_group` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- 表的结构 `group_permission`
--

CREATE TABLE `group_permission` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `dryRouteName` varchar(64) NOT NULL COMMENT '路由名称',
  `dryGroup` int(11) DEFAULT NULL,
  `dryAddtime` datetime NOT NULL COMMENT '添加时间',
  `dryNote` varchar(255) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户组权限';

--
-- 转存表中的数据 `group_permission`
--

INSERT INTO `group_permission` (`id`, `dryRouteName`, `dryGroup`, `dryAddtime`, `dryNote`) VALUES
(411, 'group_index', 3, '2017-09-12 15:52:29', 'super'),
(412, 'user_index', 3, '2017-09-12 15:52:29', 'super'),
(413, 'menu_index', 3, '2017-09-12 15:52:29', 'super'),
(414, 'grouppermission_index', 3, '2017-09-12 15:52:29', 'super'),
(415, 'choice_index', 3, '2017-09-12 15:52:29', 'super'),
(416, 'setting_index', 3, '2017-09-12 15:52:29', 'super'),
(417, 'language_index', 3, '2017-09-12 15:52:29', 'super'),
(418, 'permission_index', 3, '2017-09-12 15:52:29', 'super'),
(419, 'choiceconfig_index', 3, '2017-09-12 15:52:29', 'super'),
(420, 'tablekeyvalue_index', 3, '2017-09-12 15:52:29', 'super'),
(421, 'user_update_password', 3, '2017-09-12 15:52:29', 'super'),
(443, 'choice_index', 2, '2017-09-13 17:18:20', 'super'),
(444, 'setting_index', 2, '2017-09-13 17:18:20', 'super');

-- --------------------------------------------------------

--
-- 表的结构 `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `dryGrade` int(11) NOT NULL COMMENT '等级',
  `dryIcon` varchar(16) DEFAULT NULL COMMENT 'ICON',
  `dryName` varchar(255) NOT NULL COMMENT '名称',
  `dryUrl` varchar(255) DEFAULT NULL COMMENT 'URL',
  `dryTarget` varchar(16) NOT NULL COMMENT '打开方式',
  `dryStatus` int(11) NOT NULL COMMENT '状态',
  `dryNote` varchar(255) DEFAULT NULL COMMENT '备注',
  `drySort` int(11) NOT NULL COMMENT '排序',
  `dryDataSort` varchar(32) DEFAULT NULL COMMENT '数据排序',
  `dryAddtime` datetime NOT NULL COMMENT '添加时间',
  `dryParent` int(11) DEFAULT NULL COMMENT 'ID',
  `dryRouteName` varchar(255) DEFAULT NULL COMMENT '路由名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单';

--
-- 转存表中的数据 `menu`
--

INSERT INTO `menu` (`id`, `dryGrade`, `dryIcon`, `dryName`, `dryUrl`, `dryTarget`, `dryStatus`, `dryNote`, `drySort`, `dryDataSort`, `dryAddtime`, `dryParent`, `dryRouteName`) VALUES
(1, 1, 'fa fa-laptop', '系统管理', NULL, '_parent', 1, NULL, 2, '10011002100110021001100210011002', '2016-04-17 21:53:32', NULL, NULL),
(2, 2, NULL, '用户组', NULL, '_parent', 1, NULL, 1, '10011002100210011002100110021001', '2016-04-17 22:07:13', 1, 'group_index'),
(6, 2, NULL, '用户', NULL, '_parent', 1, NULL, 2, '10011002100210021002100210021002', '2016-04-18 22:04:30', 1, 'user_index'),
(7, 2, NULL, '菜单', NULL, '_parent', 1, NULL, 4, '10011002100210041002100410021004', '2016-04-20 22:30:50', 1, 'menu_index'),
(10, 2, NULL, '用户组权限', NULL, '_parent', 1, NULL, 3, '10011002100210031002100310021003', '2016-04-23 15:54:11', 1, 'grouppermission_index'),
(20, 2, NULL, '选项', NULL, '_parent', 1, NULL, 6, '10011002100210061002100610021006', '2016-05-29 20:50:54', 1, 'choice_index'),
(21, 2, NULL, '设置', NULL, '_parent', 1, NULL, 5, '10011002100210051002100510021005', '2016-05-29 22:18:21', 1, 'setting_index'),
(45, 1, 'fa fa-laptop', '用户中心', NULL, '_blank', 1, NULL, 1, '10011001100110011001100110011001', '2017-03-16 22:42:02', NULL, NULL),
(46, 2, NULL, '更新密码', NULL, '_parent', 1, NULL, 1, '10011001100210011002100110021001', '2017-03-16 22:42:33', 45, 'user_update_password'),
(48, 2, NULL, '语言', NULL, '_parent', 1, NULL, 10, '10011002100210101002101010021010', '2017-09-04 15:26:28', 1, 'language_index'),
(49, 2, NULL, '权限', NULL, '_parent', 1, NULL, 9, '10011002100210091002100910021009', '2017-09-06 10:05:10', 1, 'permission_index'),
(50, 2, NULL, '表键值', NULL, '_parent', 1, NULL, 8, '10011002100210081002100810021008', '2017-09-12 11:58:22', 1, 'tablekeyvalue_index'),
(51, 2, NULL, '选项配置', NULL, '_parent', 1, NULL, 7, '10011002100210071002100710021007', '2017-09-12 15:01:02', 1, 'choiceconfig_index'),
(52, 2, NULL, '用户权限', NULL, '_parent', 1, NULL, 11, '10011002100210111002101110021011', '2017-09-13 13:43:35', 1, 'userpermission_index'),
(53, 2, NULL, '缓存', NULL, '_parent', 1, NULL, 12, '10011002100210121002101210021012', '2017-09-14 11:09:46', 1, 'cache_index'),
(54, 2, NULL, '重置用户密码', NULL, '_parent', 1, NULL, 13, '10011002100210131002101310021013', '2017-09-14 14:14:14', 1, 'system_reset_user_password'),
(55, 2, NULL, '搜索配置', NULL, '_parent', 1, NULL, 14, '10011002100210141002101410021014', '2017-09-14 16:55:55', 1, 'searchconfig_index');

-- --------------------------------------------------------

--
-- 表的结构 `sessions`
--

CREATE TABLE `sessions` (
  `sess_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `sess_data` blob NOT NULL,
  `sess_time` int(10) UNSIGNED NOT NULL,
  `sess_lifetime` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- 转存表中的数据 `sessions`
--

INSERT INTO `sessions` (`sess_id`, `sess_data`, `sess_time`, `sess_lifetime`) VALUES
('1aba5fe3e8l1rnbsqc6fdsclj7', 0x5f7366325f617474726962757465737c613a353a7b733a32363a225f73656375726974792e6d61696e2e7461726765745f70617468223b733a33343a22687474703a2f2f3132372e302e302e313a3838382f61646d696e782f67726f75702f223b733a31383a225f637372662f61757468656e746963617465223b733a34333a226373556c6a76574c5a305f735a7044776237657935394d50306f57414441484d7a7a4a6e452d5a69766373223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b733a32393a225f637372662f7878666178795f64727962756e646c655f63686f696365223b733a34333a22676532467868447076524b38637a344d67797939357a6d7a5378483476617a76704a626772674d44396a41223b733a32383a225f637372662f7878666178795f7573657262756e646c655f75736572223b733a34333a22634a423036715076635931695345446642794d484b5f3435345549514e486969584850544970694954376b223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353830323832363b733a313a2263223b693a313530353739373833323b733a313a226c223b733a313a2230223b7d, 1505802826, 1440),
('1tjqggk295oe9i1dv856h7b2i7', 0x5f7366325f617474726962757465737c613a323a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a22784766693375337346417a4f6e5a634d74347466564f334757664a4f7146357353697638365f4152636930223b733a32333a225f73656375726974792e6c6173745f757365726e616d65223b733a363a22787866617879223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530363035393333313b733a313a2263223b693a313530363035383637393b733a313a226c223b733a313a2230223b7d, 1506059331, 1440),
('26dq80a7028nlr2fkngmle3un3', 0x5f7366325f617474726962757465737c613a343a7b733a32363a225f73656375726974792e6d61696e2e7461726765745f70617468223b733a34363a22687474703a2f2f3132372e302e302e313a3838382f6170705f6465762e7068702f61646d696e782f63616368652f223b733a31383a225f637372662f61757468656e746963617465223b733a34333a226d6f6f356b4e425146654b514b46784f41697a4a624372346e624a77444769685850664970544167543567223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b733a32383a225f637372662f7878666178795f7573657262756e646c655f75736572223b733a34333a22574e716f65472d746e7332457a6458434b677661755a727568574f594a6648725854496c4b4666485f7530223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353437313330393b733a313a2263223b693a313530353436393739343b733a313a226c223b733a313a2230223b7d, 1505471309, 1440),
('2i10g2oo6bt013rq4cuqi5bq44', 0x5f7366325f617474726962757465737c613a323a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a2230743373356e79354f514b6854716f5f53685f436679464b44397a4d73615f51524f4c6c30654338634b4d223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353436383634303b733a313a2263223b693a313530353436353636393b733a313a226c223b733a313a2230223b7d, 1505468640, 1440),
('3uvfql57af68iiccpb6llb6l33', 0x5f7366325f617474726962757465737c613a313a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a22594d7a7152735a452d6a644e4b54464f537466776a654b6e6d6c73485745524b6761467148744c497a6249223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353732313635343b733a313a2263223b693a313530353732313635323b733a313a226c223b733a313a2230223b7d, 1505721654, 1440),
('6s05enmt53j1eji2su680o7315', 0x5f7366325f617474726962757465737c613a323a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a223850444164652d736a597358716559596c776f5f6775727a764d454f65456e6e4f66597a6b5a7472485a45223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530363530313537363b733a313a2263223b693a313530363530313537363b733a313a226c223b733a313a2230223b7d, 1506501576, 1440),
('80rsh2jk1c8v0jbgfakqcqpi42', 0x5f7366325f617474726962757465737c613a313a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a2257596b4844684545725a596f4776545f316533344537506f7554317952586335784265686b454f42684663223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353732383531343b733a313a2263223b693a313530353732383531343b733a313a226c223b733a313a2230223b7d, 1505728514, 1440),
('fcb9bkhro2o8us7dponujhbjk1', 0x5f7366325f617474726962757465737c613a313a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a224336784f6c7939654f2d6b336f434848703441614d626e3664397759304a66497342736c506c634e374645223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353938343231303b733a313a2263223b693a313530353938343231303b733a313a226c223b733a313a2230223b7d, 1505984210, 1440),
('focaaocag29kt6ifnlqe7klhh4', 0x5f7366325f617474726962757465737c613a343a7b733a32363a225f73656375726974792e6d61696e2e7461726765745f70617468223b733a34363a22687474703a2f2f3132372e302e302e313a3838382f6170705f6465762e7068702f61646d696e782f67726f75702f223b733a31383a225f637372662f61757468656e746963617465223b733a34333a225a466d544f2d754634366131556e4e61375272446e51716e613337506c5f4f77305f31586432784933374d223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b733a32393a225f637372662f7878666178795f64727962756e646c655f63686f696365223b733a34333a2271365a78474c41346b4f6b5868497454684f344655536c59686c483770764c79644b6d62704d4d58655745223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353732353433323b733a313a2263223b693a313530353732333733353b733a313a226c223b733a313a2230223b7d, 1505725432, 1440),
('ja7na1e9kdfnagkdjjkefjcle3', 0x5f7366325f617474726962757465737c613a323a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a22647351594d412d744d72517459466239724d6c78657578583548773333355a504c543261517a5f52546763223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530363330393334363b733a313a2263223b693a313530363330393334363b733a313a226c223b733a313a2230223b7d, 1506309346, 1440),
('lh5fivut2p3bjnksklojpuvpr4', 0x5f7366325f617474726962757465737c613a323a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a227154354763756c6b554e4642644d44595444427035453065554257695550584a6775503968487546667a77223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353733303738353b733a313a2263223b693a313530353733303639393b733a313a226c223b733a313a2230223b7d, 1505730785, 1440),
('mr9r12f01uj5jqnqk090fs2s43', 0x5f7366325f617474726962757465737c613a313a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a22655738416c48313868344d4c586b3253656730384236704c7778356e545231584c4d336962444271655449223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353837343831393b733a313a2263223b693a313530353837343831393b733a313a226c223b733a313a2230223b7d, 1505874820, 1440),
('nqb24idogu9mgoijh5h8qfif02', 0x5f7366325f617474726962757465737c613a323a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a22314253325749336a5453436b6b774f686568334d685f776b4277785a496465555f5a5a4d736c454f364845223b733a32333a225f73656375726974792e6c6173745f757365726e616d65223b733a363a22787866617879223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530363035393434393b733a313a2263223b693a313530363035393337313b733a313a226c223b733a313a2230223b7d, 1506059449, 1440),
('oql63ebh3asfba4m05tf67g896', 0x5f7366325f617474726962757465737c613a323a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a22486f373179703237484f6371306b6c554c756c6f6d456f4a7271344b5947644436474e5656617a5a6d3973223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530363331313533353b733a313a2263223b693a313530363331313533353b733a313a226c223b733a313a2230223b7d, 1506311536, 1440),
('t6q1m2fadmeg8c193a26p81v62', 0x5f7366325f617474726962757465737c613a323a7b733a31383a225f637372662f61757468656e746963617465223b733a34333a224546624e3278474a2d6b4566677179304d79396c5a6f3772746870676931326e517844546a32477a616649223b733a31343a225f73656375726974795f6d61696e223b733a3635333a22433a37343a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c41757468656e7469636174696f6e5c546f6b656e5c557365726e616d6550617373776f7264546f6b656e223a3536353a7b613a333a7b693a303b4e3b693a313b733a343a226d61696e223b693a323b733a3532353a22613a343a7b693a303b433a32393a225878666178795c5573657242756e646c655c456e746974795c55736572223a3138303a7b613a383a7b693a303b733a36303a22243279243133247344395472434f77775647482f795850362e36515a656e615656704337654270354631464e6e456d454554366f33486a58726e676d223b693a313b4e3b693a323b733a353a227375706572223b693a333b733a353a227375706572223b693a343b623a313b693a353b693a313b693a363b733a31323a2273757065724071712e636f6d223b693a373b733a31323a2273757065724071712e636f6d223b7d7d693a313b623a313b693a323b613a323a7b693a303b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a31363a22524f4c455f53555045525f41444d494e223b7d693a313b4f3a34313a2253796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c65223a313a7b733a34373a220053796d666f6e795c436f6d706f6e656e745c53656375726974795c436f72655c526f6c655c526f6c6500726f6c65223b733a393a22524f4c455f55534552223b7d7d693a333b613a303a7b7d7d223b7d7d223b7d5f7366325f666c61736865737c613a303a7b7d5f7366325f6d6574617c613a333a7b733a313a2275223b693a313530353830303139343b733a313a2263223b693a313530353739383039313b733a313a226c223b733a313a2230223b7d, 1505800194, 1440);

-- --------------------------------------------------------

--
-- 表的结构 `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `dryGroup` varchar(32) NOT NULL COMMENT '分组',
  `dryName` varchar(255) NOT NULL COMMENT '名称',
  `dryValue` text COMMENT '值',
  `drySort` int(11) NOT NULL COMMENT '排序',
  `dryAddtime` datetime NOT NULL COMMENT '添加时间',
  `dryNote` varchar(255) DEFAULT NULL COMMENT '备注',
  `dryUseDetail` varchar(32) DEFAULT NULL COMMENT '详情展示',
  `dryIsJsonString` varchar(32) DEFAULT NULL COMMENT 'json数据'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设置';

--
-- 转存表中的数据 `setting`
--

INSERT INTO `setting` (`id`, `dryGroup`, `dryName`, `dryValue`, `drySort`, `dryAddtime`, `dryNote`, `dryUseDetail`, `dryIsJsonString`) VALUES
(23, 'system', 'oa_name', 'SymfonyDry开源系统', 1, '2017-09-01 14:09:46', NULL, 'no', 'no'),
(24, 'system', 'oa_copyright_text', '后台管理系统 &copy; 2016-2017 By https://www.zhouchun.net', 2, '2017-09-01 14:51:37', '', 'yes', 'no'),
(25, 'system', 'all_user_has_permission_route', '[\r\n    "user_update_password",\r\n    "system_check_permission_button"\r\n]', 4, '2017-09-08 10:05:49', '用户默认就拥有权限的路由', 'yes', 'yes'),
(26, 'system', 'all_user_no_has_permission_route', '[\r\n    "grouppermission_new",\r\n    "grouppermission_edit"\r\n]', 3, '2017-09-08 18:06:37', '不允许用户操作的路由', 'yes', 'yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choice`
--
ALTER TABLE `choice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C1AB5A92FBE303C9` (`dryParent`);

--
-- Indexes for table `dry_choice_config`
--
ALTER TABLE `dry_choice_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dry_language`
--
ALTER TABLE `dry_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dry_permission`
--
ALTER TABLE `dry_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dry_search`
--
ALTER TABLE `dry_search`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dry_table_key_value`
--
ALTER TABLE `dry_table_key_value`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dry_user_permission`
--
ALTER TABLE `dry_user_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fos_group`
--
ALTER TABLE `fos_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Indexes for table `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_583D1F3EA76ED395` (`user_id`),
  ADD KEY `IDX_583D1F3EFE54D947` (`group_id`);

--
-- Indexes for table `group_permission`
--
ALTER TABLE `group_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3784F318C7A70FF` (`dryGroup`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7D053A93FBE303C9` (`dryParent`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sess_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `choice`
--
ALTER TABLE `choice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=11;
--
-- 使用表AUTO_INCREMENT `dry_choice_config`
--
ALTER TABLE `dry_choice_config`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- 使用表AUTO_INCREMENT `dry_language`
--
ALTER TABLE `dry_language`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- 使用表AUTO_INCREMENT `dry_permission`
--
ALTER TABLE `dry_permission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- 使用表AUTO_INCREMENT `dry_search`
--
ALTER TABLE `dry_search`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=4;
--
-- 使用表AUTO_INCREMENT `dry_table_key_value`
--
ALTER TABLE `dry_table_key_value`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `dry_user_permission`
--
ALTER TABLE `dry_user_permission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `fos_group`
--
ALTER TABLE `fos_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- 使用表AUTO_INCREMENT `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- 使用表AUTO_INCREMENT `group_permission`
--
ALTER TABLE `group_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=445;
--
-- 使用表AUTO_INCREMENT `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=56;
--
-- 使用表AUTO_INCREMENT `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=27;
--
-- 限制导出的表
--

--
-- 限制表 `choice`
--
ALTER TABLE `choice`
  ADD CONSTRAINT `FK_C1AB5A92FBE303C9` FOREIGN KEY (`dryParent`) REFERENCES `choice` (`id`);

--
-- 限制表 `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD CONSTRAINT `FK_583D1F3EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_583D1F3EFE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`);

--
-- 限制表 `group_permission`
--
ALTER TABLE `group_permission`
  ADD CONSTRAINT `FK_3784F318C7A70FF` FOREIGN KEY (`dryGroup`) REFERENCES `fos_group` (`id`);

--
-- 限制表 `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `FK_7D053A93FBE303C9` FOREIGN KEY (`dryParent`) REFERENCES `menu` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
