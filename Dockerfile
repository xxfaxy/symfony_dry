FROM xxfaxy/self-alpine-nginx-php-fpm:test
ADD common/main.nginx.conf /etc/nginx/nginx.conf
ADD common/php.ini /etc/php7/php.ini
ADD common/nginx.conf /etc/nginx/conf.d/nginx.conf
ADD common/php-fpm.conf /etc/php7/php-fpm.conf
ADD common/50-setting.ini /etc/php7/conf.d/50-setting.ini
ADD common/ssl/ssl_certificate.pem /etc/nginx/ssl/ssl_certificate.pem
ADD common/ssl/ssl_certificate_key.key /etc/nginx/ssl/ssl_certificate_key.key
ADD code /app
RUN chown nginx:nginx /app -R
RUN chmod 777 /app -R
ADD runAndInit.sh /runAndInit.sh
RUN chmod +x /runAndInit.sh
